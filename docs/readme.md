# First launch

After repo and npm installs, you might want to download mongo databse from server AND '/var/www/html/truffl_live/public/uploads' folder from server to your local directory. 

# Server

Server is running on Amazon AWS, if for some reason you need to connect to it, ask Raphael for credentials.

# FTP - Connection

- Protocol: SFTP
- Host: 3.89.209.15
- Logon Type: Key file
- User: ec2-user
- Key file: check docs folder and include trufflterminal3.ppk

Will also need a passphrase before connection to server which can be found inside 'docs/passphrase.txt'

# FTP - What and how to update

Go to '/var/www/html/truffl_live' and
ONLY UPDATE THESE:
- lib
- views
- app.js

Other files do not need updates, unless you know what you are doing.
In case you mess up symlinks, this helped me once: 

$ ln -sf /var/www/html/truffl_live/lib/modules/my-truffl/public /var/www/html/truffl_live/public/modules/my-truffl


# SSH - Connection

Im using PuTTY to connect to ssh, these are the options im using:

- Host name: ec2-user@3.89.209.15
- Port: 22
- Connection type: SSH
- Plus will have to assign trufflterminal3.ppk to connect with key instead of password
- Once connected you will be asked for passphrase that you can find in 'docs/passphrase.txt'

# SSH - Restart node.js server (after files are updated)

$ forever restartall

# SSH - Download mongo database for local usage

$ mongodump -d truffl -o <directory_name>
e.g. mondogump -d truffl -o truffl_live9

p.s. Im dumping all DB data into '/var/www/html/mongodump' folder with versions. Kinda.

After downloading DB locally, use this function to install it:

$ mongorestore -d truffl <directory_name>

Might need to remove databse, in case you have it already: https://www.tutorialkart.com/mongodb/mongodb-delete-database/


# Email (for capturing form data and sending it)

- lib/truffl-forms


## If you have more questions
Email me at code@zilvinas.pro