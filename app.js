var path = require('path');


var apos = require('apostrophe')({
  shortName: 'truffl',

  // See lib/modules for basic project-level configuration of our modules
  // responsible for serving static assets, managing page templates and
  // configuring user accounts.

  // nestedModuleSubdirs: true,

  modules: {

    // Apostrophe module configuration

    // Note: most configuration occurs in the respective
    // modules' directories. See lib/apostrophe-assets/index.js for an example.

    // However any modules that are not present by default in Apostrophe must at
    // least have a minimal configuration here: `moduleName: {}`

    // If a template is not found somewhere else, serve it from the top-level
    // `views/` folder of the project
    'apostrophe-admin-bar': {
      openOnLoad: false,
      // openOnHomepageLoad: true,
      // closeDelay: 5000
    },

    'apostrophe-templates': { viewsFolderFallback: path.join(__dirname, 'views') },

    // 'apostrophe-assets': {
    //   lean: true
    // },
    // 'apostrophe-video-widgets': {
    //   player: true
    // },

    'my-truffl' : {},

    'project': {},
    'project-widgets': { extend: 'apostrophe-pieces-widgets' },

    'cases': {},
    'cases-pages': {},

    // global
    'drawer-image-widgets': {},
    // 'link-widgets': { extend: 'apostrophe-widgets' },
    'page-link-widgets': {},
    'custom-link-widgets': {},
    'truffl-partners-logos': {},
    'truffl-partners-logos-widgets': {},

    // home page
    'project-home-widgets': {},
    'drawer-projects-widgets': {},
    'drawer-explore-block-widgets': {},
    'drawer-logo-widgets': {},

    // case page
    // 'case-sections' : { }, 
    // 'case-sections-widgets' : { }, 
    'drawer-case-header-image-widgets': {},
    'drawer-case-carousel-widgets': {},
    'drawer-case-image-widgets': {},
    'drawer-case-video-widgets': {},
    'drawer-case-text-section-widgets': {},
    'drawer-case-quote-section-widgets': {},
    'drawer-case-two-columns-section-widgets': {},

    // about page
    'drawer-about-black-box-widgets': {},
    'drawer-about-service-box-widgets': {},
    'truffl-mentioned': {},
    'truffl-mentioned-widgets': {},

    // work page
    'project-work-widgets': {},

    // testing
    'drawer-widgets': {},

    // forms backend
    'truffl-forms': {},

  },


});
