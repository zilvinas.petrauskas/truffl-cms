module.exports = [
    {
        name: 'customBgColor',
        Label: 'Custom Background Color',
        type: 'color',
    },
    {
        name: 'customTextColor',
        Label: 'Custom Text Color',
        type: 'color',
    },
    {
        name: 'customTitleColor',
        Label: 'Custom Title Color',
        type: 'color',
    },
]