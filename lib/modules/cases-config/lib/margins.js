module.exports = [
    {
        name: 'marginTop',
        Label: 'Spacing Top',
        type: 'select',
        choices: [
          {
            label: 'None',
            value: '',
          },
          {
            label: 'Tiny',
            value: 'padding-xs--top',
          },
          {
            label: 'Small',
            value: 'padding-sm--top',
          },
          {
            label: 'Average',
            value: 'padding-md--top',
          },
          {
            label: 'Large',
            value: 'padding-lg--top',
          },
          {
            label: 'Extra Large',
            value: 'padding-xl--top',
          },
        ]
      },
  
      {
        name: 'marginBottom',
        label: 'Spacing Bottom',
        type: 'select',
        choices: [
          {
            label: 'None',
            value: '',
          },
          {
            label: 'Tiny',
            value: 'padding-xs--bottom',
          },
          {
            label: 'Small',
            value: 'padding-sm--bottom',
          },
          {
            label: 'Average',
            value: 'padding-md--bottom',
          },
          {
            label: 'Large',
            value: 'padding-lg--bottom',
          },
          {
            label: 'Extra Large',
            value: 'padding-xl--bottom',
          },
        ]
      }
]