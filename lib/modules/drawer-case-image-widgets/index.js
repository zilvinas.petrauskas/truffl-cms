const defaultMargins = require('../cases-config/lib/margins.js');
const customColors = require('../cases-config/lib/colors.js');

var fields = [
    {
      name: 'headerImage',
      label: 'Header Image',
      type: 'singleton',
      widgetType: 'apostrophe-images',
      options: {
        limit: 1,
        // minSize: [ 600, 600 ],
        // aspectRatio: [ 1, 1 ]
      }
    },

    {
      name: 'size',
      label: 'Size',
      type: 'select',
      choices: [
        {
          label: 'Full Width',
          value: 'full-width',
        },
        {
          label: 'Container Width',
          value: 'page-width',
        },
        {
          label: 'Half Page Width',
          value: 'half-page-width',
        },
        {
          label: 'One Third Width',
          value: 'one-third-width',
        },
      ],
    },
    {
      name: 'limitedHeight',
      label: 'Limited Image Height',
      type: 'range',
      min: 0,
      max: 100,
      step: 5,
      help: 'Percentage value of screen (horizontally, because it works with responsive stuff). If image is smalller than selected value on a slider it will have no visible effect. When left at ZERO (0) it will be disabled.'
    },
    {
      name: 'objectPosition',
      label: 'Image position',
      help: 'Images that have limited height (section above) might need vertical position readjustment',
      type: 'select',
      choices: [
        {
          label: 'Top',
          value: 'object-position-top',
        },
        {
          label: 'Center',
          value: 'object-position-center',
        },
        {
          label: 'Bottom',
          value: 'object-position-bottom',
        }
      ]
    },
    customColors[0], // custom bg

].concat(defaultMargins);

module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Image Section',
  addFields: fields,
  arrangeFields: [
    {
      name: 'info',
      label: 'Info',
      fields: ['headerImage', 'size', 'limitedHeight', 'objectPosition'],
    }, 
    {
      name: 'ccolors',
      label: 'Colors',
      fields: ['customBgColor']
    },
    {
      name: 'spacing',
      label: 'Spacing',
      fields: ['marginTop', 'marginBottom']
    }
 ],
  construct: function(self, options){

    // console.log('-- GOT?', self.replaceWidget);

    // var trashCallback = function(){
    //   console.log('--------- trashCallback');
    // }

    // self.beforeInsert = function(req, piece, options, callback){
    //   console.log('-- BEFORE INSERT');
    // }

    // self.on('apostrophe:destroy', 'testing123', async function(req){
    //   console.log('apostrophe:destroy');
    // })

    // self.afterTrash = function(req, id, trashCallback) {
    //   console.log('---- afterTrash')
    //   return setImmediate(trashCallback);
    // };
  

  }
};