apos.define('drawer-case-image-widgets', {
    extend: 'apostrophe-widgets',
    construct: function (self, options) {
      self.play = function ($widget, data, options) {
          var $elm = undefined;
          if( $widget.hasClass('appear') ){
              $elm = $widget;
          }else{
              $elm = $widget.find('.appear');
          }

          if( $elm !== undefined ){
            $elm.addClass('run');
          }
        
          window.dispatchEvent(new CustomEvent('intro:hide'));
      };
      self.beforeShow = function(self, options){
        window.dispatchEvent(new CustomEvent('intro:hide'));
      }
    }
  });
  