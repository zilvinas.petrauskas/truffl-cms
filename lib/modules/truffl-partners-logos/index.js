module.exports = {
    extend: 'apostrophe-pieces',
    name: 'partnerLogo',
    label: 'Partner Logo',
    pluralLabel: 'Partners Logos',
    arrangeFields: [
        {
         name: 'basics',
         label: 'Basics',
         fields: ['title', 'image', 'alt'],
        }
     ],
    addFields: [
      {
        name: 'image',
        label: 'Logo',
        type: 'singleton',
        required: true,
        widgetType: 'apostrophe-images',
        options: {
          limit: 1,
          minSize: [ 300, 300 ],
          aspectRatio: [ 1, 1 ]
        }
      },
      {
        name: 'alt',
        label: 'Alt',
        type: 'string',
        contextual: true,
      }
    ],
  };