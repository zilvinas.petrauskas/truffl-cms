module.exports = {
    extend: 'apostrophe-widgets',
    label: 'Case Section',
    pluralLabel: 'Case Sections',
    addFields: [
        // {
        //     name: '_case-sections',
        //     label: 'Case Sections',
        //     type: 'joinByArray',
        //     withType: 'case-sections'
        //   },
          {
            name: 'createSection',
            label: 'Create Section',
            type: 'area',
            contextual: true,
            options: {
                widgets: {
                    'drawer-case-image': {  },
                    'drawer-case-video': {  },
                    'drawer-case-carousel': {  },
                    'drawer-case-text-section' : {  },
                    'drawer-case-quote-section' : {  },
                    'drawer-case-two-columns-section' : {  }
                }
            }
        }
    ],
}