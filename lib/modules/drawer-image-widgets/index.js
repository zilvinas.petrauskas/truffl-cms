module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Image',
  addFields: [
    {
      name: 'image',
      label: 'Image',
      type: 'singleton',
      widgetType: 'apostrophe-images',
      options: {
        limit: 1,
      },
    }
  ]
};