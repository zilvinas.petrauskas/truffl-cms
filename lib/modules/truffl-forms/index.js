"use strict";
const nodemailer = require("nodemailer");
const striptags = require('striptags');
const config = require('./config.json');

var globalMailConfig = {
    to: 'hello@truffl.com',
    title: 'Truffl Automatic Mail',
    gmailAuth: {
        user: 'hello@truffl.com',
        pass: 'Truffl123',
    }
}
   

async function sendEmail(params, emailTemplateName) {

    console.log('------------ RECEIVING PARAMS --------------------');
    console.log(params);
    console.log('-----------------------------------------------');

    const mailText = prepareEmailText(secureData(params));


    let isLocalhost = false; // TODO
    let transporter;

    if(isLocalhost){

        // FOR TESTING
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        // create reusable transporter object using the default SMTP transport

        let testAccount = await nodemailer.createTestAccount();
        transporter = nodemailer.createTransport({
            host: "smtp.ethereal.email",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: testAccount.user, // generated ethereal user
                pass: testAccount.pass, // generated ethereal password
            },
        });

    }else{
        
        transporter = nodemailer.createTransport(
            {
                service: 'gmail',
                // auth: {
                //     type: 'OAuth2',
                //     user: 'mail',
                //     clientId: config.client_id,
                //     clientSecret: 'clientsecret',
                // },
                auth: {
                    user: globalMailConfig.gmailAuth.user,
                    pass: globalMailConfig.gmailAuth.pass,
                }
            }
        );

    }

    const senderName = params.name || params['first-name'] || 'Unknown sender';
    const senderEmail = params.email || params.mail;

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: `"${senderName}" <${senderEmail}>`, // sender address
        to: `${globalMailConfig.to}`, // list of receivers
        subject: `${globalMailConfig.title}`, // Subject line
        // text: text, // plain text body
        html: mailText, // html body
    }).catch(function(err){
        console.log('Error sending email', err);
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

}

function prepareEmailText(params){

    let text = [];
    for(let key in params){
        let value = params[key];
        
        let betterKey = humanReadableKey(key);
        text.push(`${betterKey}: ${value}`);
    }

    return text.join("<br>\r\n");
}

// replace "-" with a space so it looks nicer in email
function humanReadableKey(key) {
    return key.replace('-', ' ');
}

function secureData(params){

    var secure = {};
    for(let key in params){
        let value = params[key];
        if((!!value) && (value.constructor === Array)){
            value = value.join(', ');
        }
        secure[ striptags(key) ] = striptags(value);
    }

    return secure;
}

module.exports = {
    name: 'truffl-forms',
    label: 'truffl forms',
    contextualOnly: true,
    extend: 'apostrophe-widgets',

    email: {
        from: 'hello@truffl.com', // DEFAULT EMAIL FROM when not provided
    },

    construct: function (self, options) {

        // HIRE US FORM
        self.apos.app.post('/ajax/hire-us', function (req, res) {
            // 1. Access parameters in req.body
            // 2. Send back an AJAX response with `res.send()` as you normally do with Express

            sendEmail(req.body, 'emails/hire');

            return res.send({
                status: 'ok',
                parameters: req.body,
            });
        });

        // CONTACT US FORM
        self.apos.app.post('/ajax/contact-us', function (req, res) {

            sendEmail(req.body, 'emails/contact');

            return res.send({
                status: 'ok',
                parameters: req.body,
            });
        });

        // NEWSLETTERS
        self.apos.app.post('/ajax/newsletters', function (req, res) {

            sendEmail(req.body, 'emails/newsletters');

            return res.send({
                status: 'ok',
                parameters: req.body,
            });
        });

    }
}




