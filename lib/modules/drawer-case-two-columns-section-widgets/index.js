const defaultMargins = require('../cases-config/lib/margins.js');
const defaultColors = require('../cases-config/lib/colors.js');

var fields = [
  {
    name: 'left',
    label: 'Content Left',
    type: 'area',
    options: {
      widgets: {
        'drawer-case-text-section': {},
        'drawer-case-image' : {},
        'drawer-case-video' : {}
      }
    }
  },
  {
    name: 'right',
    label: 'Content Right',
    type: 'area',
    options: {
      widgets: {
        'drawer-case-text-section': {},
        'drawer-case-image' : {},
        'drawer-case-video' : {}
      }
    }
  },
  defaultColors[0], // custom bg

].concat(defaultMargins);

module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Two Columns Section',
  addFields: fields,
  arrangeFields: [
    {
      name: 'cleft',
      label: 'Content Left',
      fields: ['left']
    },
    {
      name: 'cright',
      label: 'Content Right',
      fields: ['right']
    },
    {
      name: 'ccolors',
      label: 'Colors',
      fields: ['customBgColor']
    },
    {
      name: 'spacing',
      label: 'Spacing',
      fields: ['paddingTop', 'paddingBottom']
    }
 ],
  
};