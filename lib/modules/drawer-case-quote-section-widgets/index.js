const defaultMargins = require('../cases-config/lib/margins.js');
const customColors = require('../cases-config/lib/colors.js');

var fields = [
    
  {
    name: 'quote',
    label: 'Quote',
    type: 'singleton',
    widgetType: 'apostrophe-rich-text'
  },

  {
    name: 'author',
    label: 'Author',
    type: 'singleton',
    widgetType: 'apostrophe-rich-text'
  },
  {
    name: 'customQuoteColor',
    label: 'Custom Quote Color',
    type: 'color'
  },
  {
    name: 'customAuthorColor',
    label: 'Custom Author Color',
    type: 'color',
  },
  customColors[0], // custom bg
].concat(defaultMargins);


module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Quote Section',
  addFields: fields,
   arrangeFields: [
    {
      name: 'info',
      label: 'Info',
      fields: ['quote',  'author'],
    },
    {
      name: 'colors',
      label: 'Colors',
      fields: ['customQuoteColor', 'customAuthorColor', 'customBgColor'],
    },
    {
     name: 'spacing',
     label: 'Spacing',
     fields: ['marginTop', 'marginBottom'],
    },
 ],
};