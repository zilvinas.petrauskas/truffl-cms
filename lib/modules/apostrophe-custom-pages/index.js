module.exports = {
    beforeConstruct: function(self, options) {

      options.arrangeFields = [
        {
          name: 'styling',
          label: 'Styling',
          fields: ['color1', 'color2', 'color3'],
         }
      ].concat(options.arrangeFields || []);

      options.addFields = [
        {
          type: 'boolean',
          name: 'approved',
          label: 'Approved'
        },
        {
          type: 'color',
          name: 'color1',
          label: 'Color #1',
          help: 'Primary color'
        },
        {
          type: 'color',
          name: 'color2',
          label: 'Color #2',
          help: 'Background color'
        },
        {
          type: 'color',
          name: 'color3',
          label: 'Color #3',
          help: 'Accent color'
        },
      ].concat(options.addFields || []);
    }
  };