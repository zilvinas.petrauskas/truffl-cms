const async = require('async');
const request = require('request-promise');
const _ = require('lodash');

module.exports = {
    extend: 'apostrophe-widgets',
    label: 'Projects in Work Page',
    // defer: true, // load after pages:beforeSend is complete
    arrangeFields: [
        {
         name: 'projects',
         label: 'Projects list',
         fields: ['_projects'],
        },
     ],
    addFields: [
        {
            name: '_projects',
            label: 'Projects',
            type: 'joinByArray',
            withType: 'project',
        },
    ],
}