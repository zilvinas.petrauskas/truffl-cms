const _ = require('lodash');

module.exports = {
    extend: 'apostrophe-widgets',
    label: 'Projects in Home Page',
    arrangeFields: [
        {
         name: 'projects',
         label: 'Projects list',
         fields: ['_projects'],
        },
        {
            name: 'explore',
            label: 'Explore Block',
            fields: ['exploreLabel', '_exploreLink','exploreSection', 'exploreBackgroundColor', 'exploreTextColor', 'exploreSize'],
           },
     ],
    addFields: [
        {
            name: '_projects',
            label: 'Projects',
            type: 'joinByArray',
            withType: 'project',
        },
        {
            name: 'exploreLabel',
            label: 'Label',
            type: 'string',
            required: true
          },
          {
            name: '_exploreLink',
            label: 'Link to Case Page', // FOR EXPLORE BLOCK IN HOME PAGE
            type: 'joinByOne',
            withType: 'apostrophe-page',
            idField: 'exploreLinkId',
            filters: {
              projection: {
                title: 1,
                _url: 1
              }
            }
          },
          {
            name: 'exploreSection',
            label: 'Section #',
            type: 'string',
          },
          {
            name: 'exploreBackgroundColor',
            label: 'Background Color',
            type: 'color',
          },
          {
            name: 'exploreTextColor',
            label: 'Text Color',
            type: 'color',
          },
          {
            name: 'exploreSize',
            label: 'Size',
            type: 'select',
            choices : [
              {
                label: '1/4',
                value: 'quarter'
              },
              {
                label: '1/2',
                value: 'half'
              },
            ]
          }
    ],
  
}