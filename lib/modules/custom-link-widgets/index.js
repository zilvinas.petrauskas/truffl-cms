module.exports = {
    extend: 'apostrophe-widgets',
    label: 'Custom Link',
    arrangeFields: [
        {
            name: 'info',
            label: 'Info',
            fields: ['label'],
        },
        {
         name: 'pickOne',
         label: 'Pick One',
         fields: ['custom', '_page','openNewWindow',],
        },
     ],
    addFields: [
        {
            name: 'label',
            label: 'Label',
            type: 'string',
            help: 'Make sure that when changing phone number you also update things in "Pick One" section near tel: xxxxxxx'
        },
        {
            name: 'custom',
            label: 'Custom',
            type: 'string',
            help: 'Custom property on <a> HREF attribute, can be used to trigger tel: or mailto: calls'
        },
      {
        name: '_page',
        label: 'Page',
        type: 'joinByOne',
        withType: 'apostrophe-page',
        idField: 'pageId',
        filters: {
          projection: {
            title: 1,
            _url: 1
          }
        }
      },
      {
        name: 'openNewWindow',
        label: 'Open in new window',
        type: 'boolean'
      }
    ]
}