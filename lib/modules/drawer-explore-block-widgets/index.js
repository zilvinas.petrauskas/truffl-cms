module.exports = {
    name: 'drawer-explore-block',
    extend: 'apostrophe-widgets',
    label: 'Animated Link',
    pluralLabel:  'Animated Links',
    arrangeFields: [
      {
        name: 'info',
        label: 'Info',
        fields: ['label', 'pageLink'],
      },
      {
        name: 'extra',
        label: 'Extras',
        fields: ['sectionLink','backgroundColor','textColor','size'],
      }
    ],
    addFields: [
      {
        name: 'label',
        label: 'Label',
        type: 'string',
        required: true
      },
      {
        name: 'pageLink',
        label: 'Link',
        type: 'singleton',
        widgetType: 'page-link',
      },
      {
        name: 'sectionLink',
        label: 'Section #',
        type: 'string',
      },
      {
        name: 'backgroundColor',
        label: 'Background Color',
        type: 'color',
      },
      {
        name: 'textColor',
        label: 'Text Color',
        type: 'color',
      },
      {
        name: 'size',
        label: 'Size',
        type: 'select',
        choices : [
          {
            label: '1/4',
            value: 'quarter'
          },
          {
            label: '1/2',
            value: 'half'
          },
          {
            label: 'None',
            value: '',
          }
        ]
      }
    ],

  };