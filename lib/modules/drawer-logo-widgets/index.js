module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Logo',
  addFields: [
    {
      name: 'image',
      label: 'Image',
      type: 'singleton',
      widgetType: 'apostrophe-images',
      options: {
        limit: 1,
        minSize: [ 300, 300 ],
        aspectRatio: [ 1, 1 ]
      }
    },
    {
      name: 'wrapper',
      label: 'Wrapper (leave blank if uncertain)',
      type: 'select',
      choices: [
        {
          label: 'None',
          value: undefined,
        },
        {
          label: '1/4',
          value: 'quarter',
        },
        {
          label: '1/2',
          value: 'half',
        },
      ]
    }
  ],
  
};