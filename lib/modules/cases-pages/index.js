var _ = require('@sailshq/lodash');
var async = require('async');

module.exports = {
    label: 'Case Page',
    name: 'cases-pages',
    extend: 'apostrophe-custom-pages',
    // filters:{
    //   ancestors: {
    //     children: {
    //       depth : 2,
    //       areas: ['defaultCasePageHeaderImage', 'image','images','thumbnail','apostrophe-images', 'drawer-case-header-image'],
    //     },
    //   },
    //   children: false, // case page doesnt have any children pages
    // },
    construct: function(self, options){
      
      // GET  http://localhost:3000/uploads/attachments/ckdou4hdo008m30lism4pq3wn-double-zero-header-image-2.full.png
  
    

      self.on('apostrophe-pages:beforeSend', 'fetchCasePages', async function(req) {

        if( ! req.data.hasOwnProperty('page') ) return null; // invoked at login too and throws error
        if( ! req.data.page.hasOwnProperty('_ancestors')) return null;       

        
        let caselist = undefined;
        let caselistKey = undefined;

        for(let a in req.data.page._ancestors){
          if(req.data.page._ancestors[a].type == 'caselist'){
            caselistKey = a;
            caselist = req.data.page._ancestors[a]._children;
          }
        }

        if(caselist !== undefined){

          const ids = getDocumentIds(caselist);
          if(ids.length === 0) return;
          
          await self.apos.attachments.db.find({
            docIds: {
              $in: ids,
            }
          }).toArray()
            .then(function(attachments) {

              self.apos.attachments.all(attachments, { annotate: true });

              _.each(caselist, function(c, key){
                  req.data.page._ancestors[caselistKey]._children[key].defaultCasePageHeaderImage.items[0].image.items[0].attachment = attachImageToPiece(c, attachments);
              })

              return;
            })
        }

       
      });

      function attachImageToPiece(casePage, attachements){
        let found = null;
        _.each(attachements, function(item, index){
          if(item.docIds.includes(casePage.defaultCasePageHeaderImage.items[0].image.items[0].pieceIds[0])){
            found = item;
          }
        });
        return found;
      }

      function getDocumentIds(caselist){
        const ids = []; // for ids
        // go thru list and collect widget (images) ids to load
        for(let i=0;i<caselist.length;i++){
          var piece = caselist[i].defaultCasePageHeaderImage.items[0];
          ids.push( piece.image.items[0].pieceIds[0] );
        }

        return ids;
      }

    }
  };
  