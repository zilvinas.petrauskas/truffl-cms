module.exports = {
    name: 'cases',
    label: 'Case Page',
    pluralLabel: 'Case Pages',
    contextual: true,
    addFields: [
        {
            name: 'section',
            label: 'Section',
            type: 'area',
            widgets: {
                'drawer-case-image': { options: {} },
                'drawer-case-video': { options: {} },
                'drawer-case-carousel': { options: {} },
                'drawer-case-text-section' : { options: {} },
                'drawer-case-quote-section' : { options: {} },
                'drawer-case-two-columns-section' : { options: {} }
            }
        }
    ]
}