apos.define('cases-editor-modal', {
    // extend: 'apostrophe-pieces-editor-modal',
    extend: 'apostrophe-widgets-editor-modal',
    construct: function(self, options) {

      console.log('--- MODIFIED');

      var superBeforeShow = self.beforeShow;
      self.beforeShow = function(callback) {
        self.link('makeModuleVisible', self.makeModuleVisible);
        return superBeforeShow(callback);
      };
      self.makeModuleVisible = function() {
          console.log('----- makeModuleVisible');
            $('.appear').addClass('run');
      };
    }
  });