module.exports = {
    extend: 'apostrophe-widgets',
    label: 'Service Box',
    addFields: [
      {
        name: 'title',
        label: 'Title',
        type: 'singleton',
        widgetType: 'apostrophe-rich-text',
      },
      {
          name: 'info',
          label: 'Info',
          type: 'singleton',
          widgetType: 'apostrophe-rich-text',
          required: true,
      },
      {
          name: 'boxColor',
          label: 'Box Color',
          type: 'color',
      },
      {
          name: 'textColor',
          label: 'Text Color',
          type: 'color'
      }
    
    ],
    
  };