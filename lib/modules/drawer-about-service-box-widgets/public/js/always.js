apos.define('drawer-about-service-box-widgets', {
    extend: 'apostrophe-widgets',
    construct: function (self, options) {
      self.play = function ($widget, data, options) {
          
          var $elm = $widget.hasClass('appear') ? $widget : $widget.find('.appear');
          window.dispatchEvent(new CustomEvent('widget:create', {
            detail: {
              element: $elm,
              type: '',
            }
          }));
        
      };
    }
  });
  