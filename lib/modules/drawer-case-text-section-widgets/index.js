const defaultMargins = require('../cases-config/lib/margins.js');
const defaultColors = require('../cases-config/lib/colors.js');

var fields = [
  {
    name: 'title',
    label: 'Section Title',
    type: 'singleton',
    widgetType: 'apostrophe-rich-text'
  },
  {
    name: 'customTitleColor',
    label: 'Custom Title Color',
    type: 'color',
    help: 'If you dont like default page colors then you can set a custom color here',
  },
  {
    name: 'about',
    label: 'Section Text',
    type: 'singleton',
    widgetType: 'apostrophe-rich-text'
  },
  {
    name: 'customAboutColor',
    label: 'Custom Text Color',
    type: 'color',
  },
  defaultColors[0], // custom bg
].concat(defaultMargins);

module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Text Section',
  addFields: fields,
  arrangeFields: [
    {
      name: 'info',
      label: 'Info',
      fields: ['title', 'about'],
    },
    {
      name: 'colors',
      label: 'Colors',
      fields: ['customTitleColor', 'customAboutColor', 'customBgColor'],
    },
    {
     name: 'spacing',
     label: 'Spacing',
     fields: ['marginTop', 'marginBottom'],
    },
    
 ],
};