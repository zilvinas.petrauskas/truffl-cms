module.exports = {
    extend: 'apostrophe-pieces',
    name: 'case-sections',
    label: 'Case Section',
    pluralLabel: 'Case Sections',
    contextual: true,
    addFields: [
        {
            name: 'createSection',
            label: 'Create Section',
            type: 'area',
            contextual: true,
            options: {
                widgets: {
                    'drawer-case-image': {  },
                    'drawer-case-video': {  },
                    'drawer-case-carousel': {  },
                    'drawer-case-text-section' : {  },
                    'drawer-case-quote-section' : {  },
                    'drawer-case-two-columns-section' : {  }
                }
            }
        }
    ],
    // construct(self, options){
        // Pass data.color1,2,3 to widget options?
    // }
}