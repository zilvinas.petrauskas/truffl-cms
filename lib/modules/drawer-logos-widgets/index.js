module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Logos',
  addFields: [
    {
      name: 'company',
      label: 'Company',
      type: 'string',
      required: true
    },
    {
      name: 'image',
      label: 'Image',
      type: 'singleton',
      widgetType: 'apostrophe-images',
      options: {
        limit: 1,
        minSize: [ 200, 200 ],
        aspectRatio: [ 1, 1 ]
      }
    },
    {
      name: 'cover',
      label: 'Image needs more spacing?',
      type: 'boolean',
    },
    {
      name: 'backgroundColor',
      label: 'Background Color (with transparent background only)',
      type: 'color',
    },
    {
      name: 'size',
      label: 'Block Size',
      type: 'select',
      choices: [
        {
          label: '1/2',
          value: 'half'
        },
        {
          label: '1/4',
          value: 'quarter',
        }
      ],
    },
  
  ],
  
};