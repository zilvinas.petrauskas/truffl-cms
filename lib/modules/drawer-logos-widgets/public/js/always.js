apos.define('drawer-logos-widgets', {
    extend: 'apostrophe-widgets',
    construct: function(self, options) {

      self.play = function($widget, data, options) {

        // console.log('data-drawer-logos-wrapper', $widget);

        var size = $widget.find('[data-drawer-logos-wrapper]').attr('data-size');
        // console.log('1', size);
        $widget.parents('.apos-area-widget-wrapper').addClass(size);

      };
    }
  });