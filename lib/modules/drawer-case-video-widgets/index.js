const defaultMargins = require('../cases-config/lib/margins.js');
const customColors = require('../cases-config/lib/colors.js');

var fields = [
    {
      name: 'videoObj',
      label: 'Video',
      type: 'singleton',
      widgetType: 'apostrophe-video',
      help: 'Vimeo video link should follow this format: https://player.vimeo.com/video/{VIDEO_ID} where {VIDEO_ID} is your numeric video ID',
      options: {
        player: true,
      }
    },
    // {
    //   name: 'videoThumbnail',
    //   label: 'Video Thumbnail',
    //   help: 'If not provided, it will use default thumbnail from Vimeo. MIGHT BE LOW REZOLUTION AND PIXELATED IMAGE OVER THERE.',
    //   type: 'singleton',
    //   widgetType: 'apostrophe-images',
    //   options: {
    //     limit: 1,
    //   }
    // },

    {
      name: 'size',
      label: 'Size',
      type: 'select',
      choices: [
        {
          label: 'Full Width',
          value: 'full-width',
        },
        {
          label: 'Container Width',
          value: 'page-width',
        },
        {
          label: 'Half Page Width',
          value: 'half-page-width',
        },
        {
          label: 'Width 45%',
          value: 'size-45p',
        },
        {
          label: 'Width 40%',
          value: 'size-40p',
        },
        {
          label: 'One Third Width',
          value: 'one-third-width',
        },

      ],
    },
    customColors[0], // custom bg
].concat(defaultMargins);

module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Video Section',
  addFields: fields,
  arrangeFields: [
    {
      name: 'info',
      label: 'Info',
      fields: ['videoObj', 'size', 'videoThumbnail'],
    }, 
    {
      name: 'ccolors',
      label: 'Colors',
      fields: ['customBgColor']
    },
    {
      name: 'spacing',
      label: 'Spacing',
      fields: ['marginTop', 'marginBottom']
    }
 ]
};