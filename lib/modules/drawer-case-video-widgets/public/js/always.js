apos.define('drawer-case-video-widgets', {
    extend: 'apostrophe-widgets',
    construct: function (self, options) {

      self.play = function ($widget, data, options) {

          var $elm = undefined;
          if( $widget.hasClass('appear') ){
              $elm = $widget;
          }else{
              $elm = $widget.find('.appear');
          }

          if( $elm !== undefined ){
            $elm.addClass('run');
          }
        
          window.dispatchEvent(new CustomEvent('intro:hide'));
          // window.dispatchEvent(new CustomEvent('video:play', {
          //   detail: {
          //     elem: $elm[0],
          //     widget: $widget,
          //     data: data,
          //     options: options
          //   },
          // }));
          // console.log('TRIGGER VIDEO PLAY');
      };
      self.beforeShow = function(self, options){
        window.dispatchEvent(new CustomEvent('intro:hide'));
      }
    }
  });
  
  