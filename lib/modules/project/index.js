const _ = require('lodash');

module.exports = {
    name: 'project',
    extend: 'apostrophe-pieces',
    label: 'Project',
    pluralLabel: 'Projects',
    arrangeFields: [
      {
        name: 'info',
        label: 'Info',
        fields: [
          'company', 'pageLink', 'projectType', 'tagline', 'image',
        ]
      },
      {
        name: 'workpage',
        label: 'Work page',
        fields: [
         'transitionImage', 'customposition', 'customsize','posx','posy', 'transitionEffect'
        ],
      },
      {
        name: 'styling',
        label: 'Style',
        fields: ['size', 'backgroundColor', 'textColor', 'arrowBackground', 'arrowColor']
      },
      {
        name: 'thumbnail',
        label: 'Not Yet Working',
        fields: ['thumbnail']
      },
    ],
    addFields: [
      {
        name: 'company',
        label: 'Company / Label',
        type: 'string',
        required: true
      },
      {
        name: 'pageLink',
        label: 'Case Page Link',
        type: 'singleton',
        widgetType: 'page-link'
      },
      {
        name: 'projectType',
        label: 'Project Type / Industry',
        type: 'string',
        required: true
      },
      {
        name: 'tagline',
        label: 'Tagline',
        type: 'area',
        options: {
          widgets: {
            'apostrophe-rich-text': {
              // toolbar: [ 'Bold']
            }
          }
        }
      },
      {
        name: 'image',
        label: 'Image (only for Home page)',
        type: 'singleton',
        widgetType: 'apostrophe-images',
        options: {
          limit: 1,
          // minSize: [300, 300],
          // aspectRatio: [1, 1]
        }
      },

      {
        name: 'customposition',
        type: 'boolean',
        label: 'Use Custom attributes for Image?',
        choices: [
          {
            value: true,
          }
        ]
      },
      {
        name: 'customsize',
        label: 'Image Size',
        help: 'Size and positions are relative to page width',
        type: 'range',
        min: 10,
        max: 100,
        step: 5,
        value: 50,
      },
      {
        name: 'posx',
        label: 'Custom X Position',
        type: 'range',
        min: 0,
        max: 120,
        step: 1,
        value: 0,
      },
      {
        name: 'posy',
        label: 'Custom Y Position',
        type: 'range',
        min: -50,
        max: 100,
        step: 1,
        value: 0,
      },

      {
        name: 'backgroundColor',
        label: 'Background Color',
        type: 'color',
      },
      {
        name: 'textColor',
        label: 'Text Color',
        type: 'color',
      },
      {
        name: 'size',
        label: 'Size',
        type: 'select',
        choices: [
          {
            label: '1/2',
            value: 'half'
          },
          {
            label: '1/4',
            value: 'quarter'
          },
        ]
      },
      {
        name: 'arrowBackground',
        label: 'Arrow Background',
        type: 'color',
      },
      {
        name: 'arrowColor',
        label: 'Arrow Color',
        type: 'color',
      },

      {
        name: 'transitionImage',
        label: 'Transition Image (for work page ONLY)',
        type: 'singleton',
        widgetType: 'apostrophe-images',
        options: {
          limit: 1,
        }
      },
      {
        name: 'transitionEffect',
        label: 'Transition Effect',
        help: 'Ignore this one',
        type: 'select',
        choices: [
          {
            label: "None",
            value: "",
          },
          {
            label: "Up",
            value: "transition-up",
          }
        ],
      },
      {
        name: 'thumbnail',
        label: 'Thumbnail',
        help: 'Squared image for more projects at the bottom of each case page',
        type: 'singleton',
        widgetType: 'apostrophe-images',
        options: {
          limit: 1,
        }
      },
    ],

    construct: function(self, options){


    }

  
  };