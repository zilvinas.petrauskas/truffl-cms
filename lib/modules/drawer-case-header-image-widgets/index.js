module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Header Image',
  addFields: [
   
    {
      name: 'size',
      label: 'Image Size',
      type: 'range',
      min: 10,
      max: 100,
      step: 5,
      value: 50,
    },
    {
      name: 'position',
      label: 'Image Position',
      type: 'select',
      choices: [
        {
          label: 'Default',
          value: '',
        },
        {
          label: 'Top Left',
          value: 'position-top-left',
        },
        {
          label: 'Top Center',
          value: 'position-top-center',
        },
        {
          label: 'Top Right',
          value: 'position-top-right',
        },
      ]
    },
    {
      name: 'position-x',
      label: 'Custom X Position',
      help: 'When "Image Position" is not enough. Leave "Image Position" at "Default" and modify X and Y instead.',
      type: 'range',
      min: 0,
      max: 120,
      step: 1,
      value: 0,
    },
    {
      name: 'position-y',
      label: 'Custom Y Position',
      type: 'range',
      min: -50,
      max: 100,
      step: 1,
      value: 0,
    },

    {
      name: 'image',
      label: 'Image',
      type: 'singleton',
      widgetType: 'apostrophe-images',
      options: {
        limit: 1
      },
      filters: {
        projection: {
          attachment: true,
          description: true,
          title: true
        }
      },
      controls: {
        movable: false,
        position: 'bottom-left'
      }
    },

  ],
  
};