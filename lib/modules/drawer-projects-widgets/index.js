const _ = require('lodash');

module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Project',

  // beforeConstruct: function(self, options){
  //   options.arrangeFields = [
  //     {
  //       name: 'project',
  //       label: 'project',
  //       fields: [ 'company', 'tagline', 'type', 'image', 'background-color', 'text-color', 'size' ]
  //     },
  //   ].concat( options.addFields || )
  // },

  // construct: function(self, options) {
  // var superPushAssets = self.pushAssets;
  // self.pushAssets = function() {
  //   superPushAssets();
  //   self.pushAsset('stylesheet', 'always', { when: 'always' });
  // };
  // },
  arrangeFields: [
    {
      name: 'info',
      label: 'Info',
      fields: [
        'company', '_pages', 'projectType', 'tagline', 'image', 'size',
      ]
    },
    {
      name: 'styling',
      label: 'Style',
      fields: ['backgroundColor', 'textColor', 'arrowBackground', 'arrowColor']
    },
  ],
  addFields: [
    {
      name: 'company',
      label: 'Company / Label',
      type: 'string',
      required: true
    },
    {
      name: 'pageLink',
      label: 'Link to Case Page #3',
      type: 'singleton',
      widgetType: 'page-link',
      // required: true,
      // idField: 'pageId'
    },
    {
      name: '_pages',
      // type: 'joinByArray',
      // idsField: 'pageIds',
      type: 'joinByOne',
      idField: 'pageIds',
      withType: 'apostrophe-page',
      label: 'Case Page',
      filters: {
        // areas: true,
        children: true
      }
    },
    // {
    //   name: '_page',
    //   type: 'joinByOne',
    //   withType: 'apostrophe-page',
    //   label: 'Case Page',
    //   required: true,
    //   idField: 'pageId'
    // },
    {
      name: 'projectType',
      label: 'Project Type / Industry',
      type: 'string',
      required: true
    },
    {
      name: 'tagline',
      label: 'Tagline',
      type: 'area',
      options: {
        widgets: {
          'apostrophe-rich-text': {
            // toolbar: [ 'Bold']
          }
        }
      }
    },
    {
      name: 'image',
      label: 'Image',
      type: 'singleton',
      widgetType: 'apostrophe-images',
      options: {
        limit: 1,
        // minSize: [300, 300],
        // aspectRatio: [1, 1]
      }
    },
    {
      name: 'backgroundColor',
      label: 'Background Color',
      type: 'color',
    },
    {
      name: 'textColor',
      label: 'Text Color',
      type: 'color',
    },
    {
      name: 'size',
      label: 'Size',
      type: 'select',
      choices: [
        {
          label: '1/2',
          value: 'half'
        },
        {
          label: '1/4',
          value: 'quarter'
        },
      ]
    },
    {
      name: 'arrowBackground',
      label: 'Arrow Background',
      type: 'color',
    },
    {
      name: 'arrowColor',
      label: 'Arrow Color',
      type: 'color',
    },
  ],

  construct: function (self, options) {


    // self.on('apostrophe-pages:beforeSend', 'fetchPageHeaders', async function (req) {

      // var field = _.find(options.addFields, { name: '_pages' })
      
      // req.data.pageHeaders = await self.apos.pages
      //   .find(req, {
      //     id: {
      //       $eq: field.idField
      //     }
      //   }
      //   )
      //   .toArray();

      //   self.apos.utils.log( req.data.pageHeaders );
    // });


  }
};