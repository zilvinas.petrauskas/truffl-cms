module.exports = {
    extend: 'apostrophe-pieces',
    name: 'trufflMentionedIn',
    label: 'Mentioned in',
    pluralLabel: 'Mentioned in',
    arrangeFields: [
       {
        name: 'basics',
        label: 'Basics',
        fields: ['title', 'logo'],
       }
    ],
    addFields: [
        {
            name: 'logo',
            label: 'Logo',
            type: 'singleton',
            widgetType: 'apostrophe-images',
            options: {
              limit: 1,
            }
        },
    ]
}