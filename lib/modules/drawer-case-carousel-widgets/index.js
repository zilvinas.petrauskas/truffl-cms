const defaultMargins = require('../cases-config/lib/margins.js');
const customColors = require('../cases-config/lib/colors.js');

var fields = [
  {
    name: 'images',
    label: 'Images',
    type: 'area',
    options: {
        widgets: {
          'apostrophe-images' : {
              // options: {
              //     aspectRatio: [4,3],
              // }
          }
        }
    }
  },
  customColors[0], // custom bg
].concat(defaultMargins);

module.exports = {
    extend: 'apostrophe-widgets',
    label: 'Carousel Section',
    addFields: fields
}