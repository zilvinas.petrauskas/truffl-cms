apos.define('drawer-case-carousel-widgets', {
    extend: 'apostrophe-widgets',
    construct: function (self, options) {
      self.play = function ($widget, data, options) {
     
        var parentHeight = $widget.find('.case-carousel-section')[0].style.height;
        var container = $widget.find('.swiper-container')[0];

        var s = new Swiper ( container, {
          preloadImages: false,
          lazy: true,
          loop: true,
          pagination: {
            el: '.swiper-pagination',
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          scrollbar: {
            el: '.swiper-scrollbar',
          },

          effect: 'slide',
          speed: 700,
          height: parentHeight,

          on: {
            init: function(){
              runTimer();
            },
            click: function(){
              userInteractionActive();
            },
            touchMove: function(){
              userInteractionActive();
            },
            slideChange: function(){

            }
          }

        });

        var userInteracted = false;
        var interactionTimer;
        var animationTimer;
        function userInteractionActive(){
          userInteracted = true;
          clearTimeout(interactionTimer);
          clearTimeout(animationTimer);
          interactionTimer = setTimeout(function(){
            userInteracted = false;
            runTimer();
          }, 6000);
        }
       
        function runTimer(){
          clearInterval(animationTimer);
          animationTimer = setInterval(function(){
              s.slideNext();
          }, 4000);
        }
      


      };
    }
  });
  