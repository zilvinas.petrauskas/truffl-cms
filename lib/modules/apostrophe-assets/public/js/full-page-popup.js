

function FullPagePopup(params) {

    if (!params.container) return;

    this.container = params.container;
    this.trigger = params.trigger;
    this.pagesClassName = 'popup-page';
    this.pages = [];
    this.animation = '';
    this.first = null;
    this.current = null;
    this.lastScrollPosition = 0;
    this.active = false;

    this.jQueryForm = params.jQueryForm; // { form<HtmlElmeent>, rules<Object> }
    this.jQueryForm.validator = this.validator();

    this.init();

}

FullPagePopup.prototype.init = function () {

    this.pages = this.container.querySelectorAll('.' + this.pagesClassName);

    if (this.pages.length === 0) {
        console.warn('FullPagePopup child containers not found, used class: ', this.pagesClassName);
    }

    this.first = this.pages[0];
    this.current = this.pages[0];

    if (this.animation.length > 0) this.container.classList.add(this.animation);

    for (let i = 0; i < this.pages.length; i++) {

    }

    this.bindEvents();
    
}

FullPagePopup.prototype.toggleAnimation = function (toggle) {
    if (toggle) {
        this.container.classList.add('animated');
    } else {
        this.container.classList.remove('animated');
    }
}

FullPagePopup.prototype.render = function (page) {

    for (let i = 0; i < this.pages.length; i++) {
        if (page.id == this.pages[i].id) {
            this.pages[i].classList.add('current');
        } else {
            this.pages[i].classList.remove('current');
        }
    }

}

FullPagePopup.prototype.show = function () {

    this.hideBodyOverflow(true);
    this.toggleAnimation(false);
    this.reset();
    this.render(this.current);
    this.container.classList.add('visible');
    this.active = true;
}

FullPagePopup.prototype.hide = function () {

    this.hideBodyOverflow(false);
    this.active = false;
    this.container.classList.remove('visible');
}



FullPagePopup.prototype.next = function () {

    this.toggleAnimation(true);

    var page = this.getPage(true);
    if (page) {
        this.render(page);
        this.current = page;
    } else {
        console.warn('Next page not found!')
    }


}

FullPagePopup.prototype.prev = function () {

    this.toggleAnimation(true);

    var page = this.getPage(false);
    if (page) {
        this.render(page);
        this.current = page;
    }
}





FullPagePopup.prototype.reset = function () {

    this.current = this.first;
}

FullPagePopup.prototype.hideBodyOverflow = function (hide) {

    if (hide) {

        this.lastScrollPosition = window.pageYOffset || document.body.scrollTop;
        document.body.classList.add('hide-page-overflow');

    } else {

        document.body.classList.remove('hide-page-overflow');
        window.scrollTo(0, this.lastScrollPosition);
    }
}

FullPagePopup.prototype.getPage = function (next) {

    var page = null;
    for (let i = 0; i < this.pages.length; i++) {

        if (this.pages[i].id == this.current.id) {

            if (next) {

                if (typeof this.pages[i + 1] !== 'undefined') {
                    page = this.pages[i + 1];
                }

            } else {

                if (typeof this.pages[i - 1] !== 'undefined') {
                    page = this.pages[i - 1];
                }
            }

            break;


        }
    }

    if (page === null) {
        console.warn('Failed to get page', next, this.current.id);
    }

    return page;
}


FullPagePopup.prototype.bindEvents = function () {

    const self = this;

    this.trigger.addEventListener('click', () => {
        self.reset();
        self.show();
    });


    var buttonsNext = this.container.querySelectorAll('.page-button--next');
    for (let i = 0; i < buttonsNext.length; i++) {
        buttonsNext[i].addEventListener('click', () => {

            if (!this.active) return;
            this.next();
        })
    }

    var closePopup = this.container.querySelectorAll('.popup-page-close');
    for (let i = 0; i < closePopup.length; i++) {
        closePopup[i].addEventListener('click', () => {
            if (!this.active) return;
            this.hide();
        })
    }

    var submitButtons = this.container.querySelectorAll('.page-button--submit button');
    for (let i = 0; i < submitButtons.length; i++) {

        submitButtons[i].addEventListener('click', () => {
            if (!this.active) return;
            this.checkForm();
        })
    }

}

FullPagePopup.prototype.thankyou = function () {
    this.next();
}

FullPagePopup.prototype.checkForm = function () {
    var valid = this.jQueryForm.validator.form();
    console.log('valid?', valid);
    if(valid === true){
        this.submit();
    }
}


FullPagePopup.prototype.validator = function () {

    const scope = this;
    const errorContainer = '.error-container';

    return this.jQueryForm.form.validate({
        rules: this.jQueryForm.rules,
        errorClass: "invalid",
        focusInvalid: true,
        submitHandler: function (f) {}, // called manually after checkForm()
        highlight : function(element, errorClass, validClass){
            var parent = $(element).parents(errorContainer);
            if(parent.length === 0) parent = $(element);
            parent.addClass(errorClass);
        },
        unhighlight : function(element, errorClass, validClass){
            var parent = $(element).parents(errorContainer);
            if(parent.length === 0) parent = $(element);
            parent.removeClass(errorClass);
        },
        errorPlacement: function(error, element) {} // prevent creating html elements for error, only highlight them
    });
}

FullPagePopup.prototype.submit = function () {

    const scope = this;

    if (typeof this.jQueryForm === undefined || this.jQueryForm.form.length === 0) {
        console.warn('-- FORM NOT FOUND', this.jQueryForm);
        return;
    }

    var action = this.jQueryForm.form[0].dataset.action;
    var input = this.jQueryForm.form.serialize();

    console.log('-- Serialize input', input);
    console.log('-- sending form to ', action);

    $.post(action, input, function (result) {
        if (result.status === 'ok') {
            scope.thankyou();
        } else {
            console.log('--- CHECK RESPONSE', result);
        }
        return false;
    });
    return false;
}

FullPagePopup.prototype.resetForm = function () {}