var ABOUT_DEBUG_INDICATORS = false;

document.addEventListener('DOMContentLoaded', function(){
    // TODO - init mentioned list slideshows

    if( $('.about-page').length === 0) return;

    if( ! isLocalhost() ) ABOUT_DEBUG_INDICATORS = false;

    initMentionedListAnimation('.mentioned-list .mentioned-item');
    aboutPageAnimations();
    animatedPartnersLogos({
        elements: document.querySelectorAll('.animated-partners-logos .logo-animated'),
    });
    scrollOnServiceBoxHover();
})

function scrollOnServiceBoxHover(){

    var last = null;
    var history = [];
    var elements = document.querySelectorAll('.services-list .service-box');
    var timeout;
    var isScrolling = false;
    var scrollTimeout;
    var scrollBy = 20;

    window.addEventListener('scroll', function(){
        isScrolling = true;
        clearTimeout(scrollTimeout);
        scrollTimeout = setTimeout(function(){
            isScrolling = false;
        }, 100);
    })

    for(let i=0;i<elements.length;i++){
        elements[i].addEventListener('mouseenter', function(event){
            mouseEnterEvent(elements[i], event);
        });
        // elements[i].addEventListener('mouseleave', function(event){
        //     mouseLeaveEvent(elements[i], event);
        // });
    }

    function mouseEnterEvent(elem){
        
        var id = elem.dataset.service;
        
        clearTimeout(timeout);

        if( history.includes( id ) ){
            animatePageMove(elem, scrollBy);
        }else{
            history.push(id);
            if(history.length > 2) history.shift();
        }

        last = id;

    }

    function mouseLeaveEvent(elem){
        var id = elem.dataset.service;
        if(last === id){
            timeout = setTimeout(function(){
                animatePageMove(elem, -scrollBy);
            }, 100);
           
        }
    }

    function animatePageMove(elem, value){
       if(!isScrolling) $("html, body").animate({scrollTop: window.scrollY + value }, 350);
    }

}


function initMentionedListAnimation(query){

    var config = {
        delay: 3000, // ms, when next item is shown
        className: 'active',
        query : query,
        active: true,
    }

    var elements = document.querySelectorAll(query);
    var current = 0;
    var prev = 0;

    if(elements.length === 0){
        console.warn('Missing animated items: ', query, elements);
        return;
    }

    var list = [];
    for(let i=0;i<elements.length;i++) list.push( elements[i] );
    

    activate();

    function activate(){

        if(prev !== current){
            list[current].classList.remove(config.className);
        }

        prev = current;
        current =  list.length - 1 >= current+1 ? current + 1 : 0;
        list[current].classList.add(config.className);

        if(list.length > 1){
            if( checkIfActive() ){
                setTimeout(activate, config.delay);
            }else{
                setTimeout(function(){
                    initMentionedListAnimation(config.query);
                }, config.delay / 2);
               
            }
            
        }
        
    }

    // for apos update
    function checkIfActive(){
        var active = document.querySelectorAll(config.query+'.'+config.className);
        return active.length > 0;
    }
}


function aboutPageAnimations(){

    // var controller = new ScrollMagic.Controller({
    //     refreshInterval: 20,
    // });
    var vh = window.innerHeight;
    var vw = window.innerWidth;

    animateParallax();

    animatePulseText();
    // animatePulsing();

    animateBigLetters();
                            
    animateSteps(".dig-explore-execute .row.first", ".dig-explore-execute .row.first");
    animateSteps(".dig-explore-execute .row.second", ".dig-explore-execute .row.second");
    animateSteps(".dig-explore-execute .row.third", ".dig-explore-execute .row.third");

    animateTruffleParallax();


    function animatePulseText(){
        
        var scene = new ScrollMagic.Scene({
            triggerElement: ".pulse-message",
            offset: -window.innerHeight / 3,
        })
        .setClassToggle(".pulse-message", "progress")
        .addTo(ScrollMagicController);

        if(ABOUT_DEBUG_INDICATORS) scene.addIndicators();
    }

    function animatePulsing(){
        var scene = new ScrollMagic.Scene({
            triggerElement: ".pulse-container",
            offset: -vh/2
        })
        .setClassToggle(".pulse-container", "pulsing")
        .addTo(ScrollMagicController);

        if(ABOUT_DEBUG_INDICATORS) scene.addIndicators();
    }

    function animateBigLetters(){
        var scene = new ScrollMagic.Scene({
            triggerElement: "#weGetDeep",
            offset: -window.innerHeight / 5,
        })
        // trigger animation by adding a css class
        .setClassToggle(".letters-with-shadow", "progress")
        // .addIndicators({name: "1 - animate"}) // add indicators (requires plugin)
        .addTo(ScrollMagicController);

        if(ABOUT_DEBUG_INDICATORS) scene.addIndicators();
    }

    function animateSteps(triggerAt, classToggle){

        var scene = new ScrollMagic.Scene({
            triggerElement: triggerAt,
            offset: -window.innerHeight / 4
        })
        .setClassToggle(classToggle, "progress")
        .addTo(ScrollMagicController);

        if(ABOUT_DEBUG_INDICATORS) scene.addIndicators();
    }

    function animateParallax(){            

        // scene #1 - holds images #1 and #2 in place while accelerating title scroll
        var scene1 = new ScrollMagic.Scene({
                triggerElement: ".hero.container", 
                duration: vh, 
            })
            .setTween(
                new TimelineMax ()
                .add([
                    TweenMax.to(".parallax-image[data-image='1']", 1, { y: vh * 0.1, ease: Linear.easeNone}),
                    TweenMax.to(".parallax-image[data-image='2']", 1, { y: vh * 0.1, ease: Linear.easeNone}),
                    TweenMax.to("#aboutPageHeroTitle", 1, { y: vh * -0.75, ease: Linear.easeNone}),
                ])
            )
            .addTo(ScrollMagicController);
            if(ABOUT_DEBUG_INDICATORS) scene1.addIndicators();

        // scene #2 - accelerates subtitle scroll when its in view
        var scene2 = new ScrollMagic.Scene({
                triggerElement: ".hero.container", 
                duration: vh * 0.4, 
                offset: vh * 0.8
            })
            .setTween(
                new TimelineMax ()
                .add([
                    TweenMax.to(".parallax-image[data-image='2']", 1, { y: vh * 0.1, ease: Linear.easeNone}),
                    TweenMax.to(".parallax-image[data-image='3']", 1, { y: vh * 0.1, ease: Linear.easeNone}),
                    TweenMax.to(".parallax-image[data-image='4']", 1, { y: vh * -0.15, ease: Linear.easeNone}),
                    TweenMax.to("#aboutPageSubheroTitle", 1, { y: vh * -0.2, ease: Linear.easeNone}),
                ])
            )
            .addTo(ScrollMagicController);
            if(ABOUT_DEBUG_INDICATORS) scene2.addIndicators();

        // accelerates scroll of all elements
        var scene3 = new ScrollMagic.Scene({
            triggerElement: ".hero.container", 
            duration: vh * 0.5, 
            offset: vh * 1.25
        })
            .setTween(
                new TimelineMax ()
                .add([
                    TweenMax.to(".parallax-image[data-image='2']", 1, { y: vh * -0.3, ease: Linear.easeNone}),
                    TweenMax.to(".parallax-image[data-image='3']", 1, { y: vh * -0.1, ease: Linear.easeNone}),
                    TweenMax.to(".parallax-image[data-image='4']", 1, { y: vh * -0.5, ease: Linear.easeNone}),
                    TweenMax.to("#aboutPageSubheroTitle", 1, { y: vh * -0.2, ease: Linear.easeNone}),
                ])
            )
            .addTo(ScrollMagicController);
            if(ABOUT_DEBUG_INDICATORS) scene3.addIndicators();

     
    }

    function animateTruffleParallax(){

        var scene1 = new ScrollMagic.Scene({
            triggerElement: ".secret-ingredient .vertical-text-wrapper", 
            duration: vw * 1.1, 
        })
        .setTween(
            new TimelineMax ()
            .add([
                // TweenMax.to(".secret-ingredient .vertical-text-wrapper .vertical-text", 1, { y: vh * 0.2, ease: Linear.easeNone}),
                TweenMax.to(".secret-ingredient .truffles[data-truffl='1']", 1, { y: vh * -0.1, ease: Linear.easeNone}),
                TweenMax.to(".secret-ingredient .truffles[data-truffl='2']", 1, { y: vh * -0.3, ease: Linear.easeNone}),
                TweenMax.to(".secret-ingredient .truffles[data-truffl='3']", 1, { y: vh * 0.2, ease: Linear.easeNone}),
                TweenMax.to(".secret-ingredient .truffles[data-truffl='4']", 1, { y: vh * -0.5, ease: Linear.easeNone}),
            ])
        )
        .addTo(ScrollMagicController);
        if(ABOUT_DEBUG_INDICATORS) scene1.addIndicators();


        var scene2 = new ScrollMagic.Scene({
            triggerElement: ".secret-ingredient .delicious", 
            duration: vh * 1.5, 
            offset: vh * -0.25,
        })
        .setTween(
            new TimelineMax ()
            .add([
                TweenMax.to(".secret-ingredient .real-truffles", 1, { y: vh * 0.1, ease: Linear.easeNone}),
                TweenMax.to(".truffles[data-truffl='11']", 1, { y: vh * -0.1, ease: Linear.easeNone}),
                TweenMax.to(".secret-ingredient .truffles[data-truffl='12']", 1, { y: vh * -0.3, ease: Linear.easeNone}),
                TweenMax.to(".secret-ingredient .truffles[data-truffl='13']", 1, { y: vh * -0.1, ease: Linear.easeNone}),
            ])
        )
        .addTo(ScrollMagicController);
        if(ABOUT_DEBUG_INDICATORS) scene2.addIndicators();
    }
}
