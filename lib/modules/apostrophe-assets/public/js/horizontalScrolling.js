
function horizontalScrolling(config){
    
    this.config = config;
    this.lastScrollTop = 0;
    this.lastScrollDir = 'down';

    this.init();
}

horizontalScrolling.prototype.init = function(){

    this.bindEvents();
}

horizontalScrolling.prototype.bindEvents = function(){

    var wheelEvent = normalizeWheel.getEventType();
    window.addEventListener(wheelEvent, this.onScroll.bind(this), {passive: false});
    // window.addEventListener('touchmove', this.onScroll.bind(this), {passive: false});
}

horizontalScrolling.prototype.getActiveElement = function(){


    var visibleElements = [];

    for(var i=0;i<this.config.length;i++){
        if( this.horizontalScrollVisible(this.config[i].element, i, this.config) ){
            visibleElements.push( this.config[i].element );
        }
    }

    var currentHorElm = undefined;

    if(visibleElements.length === 1){
        currentHorElm = visibleElements[0];
    }else if( visibleElements.length > 1){
        if( this.isScrollingDown() ){
            currentHorElm = visibleElements[0];
        }else{
            currentHorElm = visibleElements[ visibleElements.length - 1 ];
        }
    }

    return currentHorElm;
}

horizontalScrolling.prototype.onScroll = function(event){

    if(window.innerHeight <= 800 && typeof window.orientation !== 'undefined') return true; // mobile

      // TODO - add requestAnimationFrame with busy

      this.lastScrollTop = window.scrollY || event.clientY;

      var currentHorElm = this.getActiveElement();
      if(currentHorElm === undefined) return false;

      var delta = this.getScrollDelta(event);

      var endOfHorizontalScroll = this.scrollHorizontally(currentHorElm, delta);

      if( ! endOfHorizontalScroll ){
        event.preventDefault();
        window.scrollTo(0, this.lastScrollTop)
      }

      return true;
}

horizontalScrolling.prototype.getScrollDelta = function(event){

    var normalizedEvent = normalizeWheel(event); // external lib. scroll normalization

      var multiplier = 15;
      if( UserAgent_DEPRECATED.webkit() ) multiplier = 30;
      else if( UserAgent_DEPRECATED.safari() ) multiplier = 15; // 10? need to test
      
      var delta =  normalizedEvent.spinY * multiplier;

      return delta;
}

horizontalScrolling.prototype.horizontalScrollVisible = function(elm, i, config){

    if(typeof elm === 'undefined'){
        console.warn('Missing elem', elm,i ,config);
        return;
    }

    var threshold = (window.innerHeight - elm.clientHeight) / 4

    var visible = this.checkElementVisible(elm, threshold);
    // if(visible && !elm.classList.contains('hi')){
    //     elm.classList.add('hi')
    // }else if( ! visible && elm.classList.contains('hi')){
    //     elm.classList.remove('hi')
    // }
    return visible;
}

horizontalScrolling.prototype.isScrollingDown = function(){
    var st = window.pageYOffset || document.documentElement.scrollTop;
    lastScrollDir = st > this.lastScrollTop ? 'down' : 'up';
    this.lastScrollTop = st <= 0 ? 0 : st; // for mobile or negative scrolling

    return lastScrollDir == 'down';
}

horizontalScrolling.prototype.scrollHorizontally = function(elm, delta){

    elm.scrollLeft += delta;

    var scrollLeftMax = elm.scrollLeftMax || elm.scrollWidth - elm.clientWidth;

    if( elm.scrollLeft >= scrollLeftMax ){
        return true; // reached the end of horizontal scroll
    }else if( elm.scrollLeft <= 0){ // at the start of hroizontal scroll
        return true;
    }
   
    return false;
    
}

// elemenet visible on screen?
horizontalScrolling.prototype.checkElementVisible = (elm, threshold)=>{

    threshold = threshold || 0;

    var rect = elm.getBoundingClientRect();
    var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    var below = !(rect.bottom < 0);
    var above = !(rect.top - viewHeight >= 0);

    var fullyVisible = rect.top >= 0 && rect.bottom <= window.innerHeight;

    return  above && below && fullyVisible;
}
