window.addEventListener('load', function () {

    if ($('.case-study-page').length === 0) return;

    // resizeEmbeddedVideos();
    scrollOtherWorkList();
    fitTitles();

});

function fitTitles(){

    var complicatedPages = [
        'counterpart',
    ];
    var needResize = false;

    for(let k in complicatedPages){
        if( location.href.includes(complicatedPages[k]) ){
            needResize = true;
            break;
        }
    }

    if(needResize){
        $(".header-title .apos-rich-text > p").fitText(0.7);
    }
   

}

function resizeEmbeddedVideos(){

    // Find all YouTube videos
    // Expand that selector for Vimeo and whatever else
    var $allVideos = $(".case-video-container"),

    // The element that is fluid width
    $fluidEl = $("body");

    // Figure out and save aspect ratio for each video
    $allVideos.each(function() {

        $(this)
            .data('aspectRatio', this.height / this.width)

            // and remove the hard coded width/height
            .removeAttr('height')
            .removeAttr('width');

    });

    // When the window is resized
    $(window).resize(function() {

    var newWidth = $fluidEl.width();

    // Resize all videos according to their own aspect ratio
    $allVideos.each(function() {

        var $el = $(this);
        $el
        .width(newWidth)
        .height(newWidth * $el.data('aspectRatio'));

    });

    // Kick off one resize to fix all videos on page load
    }).resize();


   
}

function scrollOtherWorkList(){

    var container = document.querySelectorAll('#scrollableMoreWork')[0];
    if(typeof container === 'undefined' || container.length === 0) return; // wrong page?

    var target = document.querySelectorAll('#scrollableMoreWork .scroll-cases-until-here')[0];

    var scrollableAreaWidth = container.scrollLeftMax || container.scrollWidth - container.clientWidth;

    var workItemSize = $('.work-item').width();
    var targetPosition = target.offsetLeft - workItemSize/2;

    if(targetPosition > scrollableAreaWidth || targetPosition <= 0) return;
    container.scrollLeft = targetPosition;

}