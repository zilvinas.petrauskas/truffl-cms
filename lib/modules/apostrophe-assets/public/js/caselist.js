
var filtersData = {};

window.addEventListener('load', function () {

    if ($('.caselist-page').length === 0) return;

    // bindButtons('.animated-arrow-link');
    // handleDynamicLoad();


});


function handleDynamicLoad() {


    // dynamicPageLoad(apos, '.animated-arrow-link');
    var dpl = dynamicPageLoad(apos, '.dynamic');
    $('.dynamic').on('click', function (e) {

        var elem = $(this);

        e.preventDefault();
        var href = elem.attr('data-href').split('/');
        var shortLink = href[href.length - 1];
        $("#casePageContent").attr('data-apos-ajax-context', shortLink);

        var currentHref = window.location.href.replace(/[?#].*$/, '');
        console.log('-currentHref',currentHref);
        elem.attr('href', currentHref)

        var sendHref = window.location.origin + window.location.pathname + '/' + shortLink;
        console.log('-sendHref', sendHref);

        $('.filterable.current').removeClass('current');
        elem.parents('.case-study-page').addClass('current');

        setTimeout(function () {
            dpl.ajaxClickHandler(e, sendHref, function () {
                setTimeout(function () {
                    $('.filterable:not(.current)').removeClass('visible');
                    $('#casePageContent').addClass('appear');
                }, 100);
            });
        }, 1100);
    });

}

/*
    1. initialize dynamic creation
    2. parse filters and update visible projects
    3. parse url and set filter and visible projects on page reload
    4. 
*/

var WORK_PAGE_FILTERS = {
    total: 2,
    loaded: 0,
    allLoaded: function(){
        return this.total === this.loaded;
    }
}

function initFilter(config) {

    // console.log('INIT', config.data);
    var cachedFilterableData = {};
    
    // extras
    config.optionsSeparator = "|";
    config.cleanUpRegex = /[^A-Za-z0-9]+/g;
    if(config.parentElemQuery){
        config.parentElem = config.elem.parents(config.parentElemQuery);
    }
    filtersData[config.key] = [];

    initializeMultiselect();


    function initializeMultiselect() {

        config.elem.multiselect(defaultMultiselectOptions());
        if (config.parse) config.data = config.data.concat(parseRawData(config.parse));
        config.data = checkConfigData(cleanUp(config.data), config.key);
        config.elem.multiselect('loadOptions', config.data);

        WORK_PAGE_FILTERS.loaded++;
        if(WORK_PAGE_FILTERS.allLoaded()) filterCases(filtersData); // prevent it triggering multiple times on page load

        resizeFilterPopup();
    }

    function resizeFilterPopup(){
        
        var popup = config.parentElem.find('.ms-options');
        var parent = config.parentElem;

        resizeParent();
        window.addEventListener('resize', resizeParent);

        function resizeParent(){
            parent.css('min-width', popup.outerWidth()+'px');
        }
    }

    function cleanUp(data) {
        if (data.lenght === 0) return data;
        for (var i = 0; i < data.length; i++) {
            var oldValue = data[i].value;
            data[i].value = cleanValue(oldValue);
        }
        return data;
    }

    function checkConfigData(data, key) {

        const urlParams = new URLSearchParams(window.location.search);
        // console.log('url params?', urlParams.get('projects'));
        // console.log('url params?', urlParams.get('tags'));

        // compare to url params and update 'checked' value where needed
        var urlParamsString = urlParams.get(key);
        if (urlParamsString !== null) {
            var params = urlParamsString.split(config.optionsSeparator);
            if (params.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    if (params.includes(data[i].value)) {
                        data[i].checked = true;
                        // console.log('-- updating', data[i].value, data[i].checked);
                        filtersData[key].push( data[i].value );
                    }
                }
            }
        }

        return data;
    }

    /*
        create data for options creation
        options{[name<string>, value<string>, checked<boolean>], ...}
    */
    function parseRawData(raw) {

        var unique = Array.from(new Set(raw));
        var data = [];

        unique.forEach(item => {
            data.push({
                name: item.replace(/&amp;/g, '&'), // bug with & being converted to &amp;
                value: item,
                checked: false,
            });
        });

        return data;
    }

    // ISSUE - text filtering needs spacing, while default filtering doesnt
    function defaultMultiselectOptions() {
        return {
            texts: {
                placeholder: 'All',
            },
            onOptionClick: function (element, option) {

                var thisOpt = $(option);
                var value = cleanValue(thisOpt.val());

                console.log(value + ' ' + (thisOpt.prop('checked') ? '' : 'de') + 'selected');

                var selected = thisOpt.prop('checked');
                if (selected) {
                    filtersData[config.key].push(value);
                } else {
                    var index = filtersData[config.key].indexOf(value);
                    if (index > -1) filtersData[config.key].splice(index, 1);
                }

                updateFiltersUrl(filtersData);
                filterCases(filtersData);
            }
        }
    }

    

    function cleanValue(value) {
        return value.replace(/&amp;/g, '').replace(config.cleanUpRegex, '').toLowerCase();
    }

    /*
    @returns: string
    */
    function cleanValueFromElem(elem){

        if(typeof elem === undefined) return '';
        return elem.text().replace(config.cleanUpRegex, '').trim().toLowerCase();
    }

    function updateFiltersUrl(data) {


        var separator = config.optionsSeparator;
        var urlParamsArray = [];

        for (var key in data) {
            var array = data[key];
            var strings = array.map(v => encodeURIComponent(cleanValue(v))).join(separator);
            // var strings = array.map(v => cleanValue(v)).join(separator);
            if (strings.length > 0) urlParamsArray.push(`${key}=${strings}`);
        }

        // console.log('url params', urlParamsArray);

        var urlParams = urlParamsArray.join("&");
        var l = window.location;
        var url = l.protocol + '//' + l.host + l.pathname;
        if (urlParams.length > 0) url += "?" + urlParams;

        if (window.history.replaceState) {
            //prevents browser from storing history with each change:
            window.history.replaceState(data, window.title, url);
        }

    }


   

    // TODO : textSearch <boolean>
    function filterCases(filters) {

        console.log('filters', filters);

        var filterCount = 0;
        for (var k in filters) {
            if (filters[k].length > 0){
                filterCount++;
                break; // its enough to have one filter for things to not be automatically visible
            }
        }

        var visibleElements = {}; // stores elements that are already visible and will no be checked 2nd time

        $('.filterable').each(function () {

            var elem = $(this);
            var _id = elem.attr('data-id');

            // #1
            if (filterCount === 0) {
                console.log('no filters - auto visibility');
                changeCaseVisibility(elem, true);
                return;
            }

            var filterableData;
            if( cachedFilterableData.hasOwnProperty(_id) ){
                filterableData = cachedFilterableData[_id];
            }else{
                filterableData = {
                    services: {
                        data: cleanValueFromElem(elem.find('.project-services')), // TODO - services text field?
                        filterBy: 'string',
                    },
                    industry: {
                        data: cleanValue(elem.attr('data-industry')),
                    }
                }
                cachedFilterableData[_id] = filterableData;
            }

            
            
            if( !visibleElements.hasOwnProperty(_id) ){
                visible = visibleElements[_id] = filterCount === 0;
            }

            for (var key in filters) {

                // if item is already visible, or no filters are selected
                if (filters[key].length === 0) {
                    continue;
                }
                if(visibleElements[_id] === true){
                    break;
                }

                if(filterableData[key].filterBy == 'string'){ // exception, string comparison
                    filters[key].forEach(function(item){
                        if (filterableData[key].data.includes( item ) || item == filterableData[key].data ) {
                            visibleElements[_id] = true;
                            visible = true;
                        }
                    })
                   
                }else{// by value
                    // array comparison
                    if (filters[key].includes(filterableData[key].data) ) {
                        visibleElements[_id] = true;
                        visible = true;
                    }
                }
                

            }

            changeCaseVisibility(elem, visible);

        })

        console.log('visibleElements', visibleElements);
    }

    function changeCaseVisibility(elem, visible) {
        if (visible) {
            elem.addClass('visible');
        } else {
            elem.removeClass('visible');
        }
    }
}



function bindButtons(query) {

    $(window).on('popstate', function (event) {
        onButtonClick(lastElem, true);
    });

    var lastElem;
    var elements = document.querySelectorAll(query);
    for (let i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', function (evt) {
            evt.preventDefault(); // TODO - manualyl redirect when animation is complete
            onButtonClick(elements[i], false);
        })
    }


    function onButtonClick(elem, cameBack) {

        lastElem = elem;
        var parent;
        if (elem.classList.contains('case-study-page')) {
            parent = $(elem);
        } else {
            parent = $(elem).parents('.case-study-page');
        }

        parent.toggleClass('active transitioning');

        var speed = 700; // ms
        var heroSection = $('.hero-section')

        if (cameBack) {
            toggleHeroSection(heroSection, 0);
            expandContentContainer();
            scrollToElementsTop(parent, 0);
            cleanDynamicDiv();
        } else {
            scrollToElementsTop(parent, speed);
            toggleHeroSection(heroSection, speed);
            expandContentContainer();
        }

    }

    function expandContentContainer() {
        var elem = $('#casePageContent');
        elem.toggleClass('expand');
    }

    function toggleHeroSection(heroSection, speed) {
        if (heroSection.is(':visible')) {
            setTimeout(function () {
                heroSection.css('display', 'none');
                $('html, body').stop().animate({ scrollTop: 0 }, 0);
            }, speed)
        } else {
            heroSection.css('display', 'flex');
        }
    }

    function cleanDynamicDiv() {
        setTimeout(function () {
            $('#casePageContent').html('');
        }, 100);
    }

    function scrollToElementsTop(jqElem, delay, callback) {

        var offset = jqElem.offset();
        $('html, body').stop().animate({ scrollTop: offset.top }, delay);
        if (callback) {
            setTimeout(function () {
                callback();
            }, delay)
        }

    }

    function hideHeroSection() {
        $('.hero-section').toggleClass('hide-gracefully');
    }

    function hideOtherSections($not, speed) {

        $('.case-study-page').each(function () {
            var $section = $(this);
            if ($section.attr('id') === $not.attr('id')) {
                console.log('found the one')
            } else {
                $section.hide(speed);
            }
        })

    }
}
