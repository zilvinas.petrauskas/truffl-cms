$(function(){

    if( $('.meet-raph').length === 0) return;

    initNewslettersForm();

})


function initNewslettersForm(){

    var form = $('#newsletters-form');
    var errorContainer = '.error-container';

    var validator = form.validate({
        rules: {
            name: {
                required: true,
                minlength: 2,
            },
            email: {
                required: true,
                email: true,
                minlength: 5
            }
        },
        errorClass: "invalid",
        focusInvalid: true,
        submitHandler: function (f) {}, // called manually after checkForm()
        highlight : function(element, errorClass, validClass){
            var parent = $(element).parents(errorContainer);
            if(parent.length === 0) parent = $(element);
            parent.addClass(errorClass);
        },
        unhighlight : function(element, errorClass, validClass){
            var parent = $(element).parents(errorContainer);
            if(parent.length === 0) parent = $(element);
            parent.removeClass(errorClass);
        },
        errorPlacement: function(error, element) {} // prevent creating html elements for error, only highlight them
    })

    $('#newsletters-button').on('click', function(){
        var isValid = validator.form();
        if(isValid) submitForm();
    })

    function submitForm(){

        var action = form[0].dataset.action;
        var input = form.serialize();
    
        console.log('-- Serialize input', input);
        console.log('-- sending form to ', action);
    
        $.post(action, input, function (result) {
            if (result.status === 'ok') {
                showThankYou();
            } else {
                console.log('--- CHECK RESPONSE', result);
            }
            return false;
        });
        return false;
    }

    function showThankYou(){
        $('#newsletter').addClass('show-thank-you');   
    }
}