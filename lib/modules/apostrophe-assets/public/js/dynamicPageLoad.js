var dynamicPageLoad = function (apos, query) {

    var self = this;
    var triggerElements = $(query);

  
  
      self.ajaxClickHandler = function(event, _href, callback) {
        var $anchor = $(this);
       
        var href = _href;
        var ajaxContext = self.ajaxContextOf($anchor);
        self.ajaxGo(ajaxContext, href, null, {}, callback);
        return false;
      };
  
      self.ajaxContextOf = function($el) {
          return $('#casePageContent').attr('data-dynamic-content');
      };
  
      self.ajaxGo = function(name, url, data, options, callback) {
        data = data || {};
        var $old = $('[data-dynamic-content="' + name + '"]');
        var $appendTo = $old.find('[data-dynamic-append]');
        var append = false;
        var hasdynamicPage = false;
        var oldTop;
  
        if (url.match(/[?&]append=1/)) {
          append = true;
        }
  
        if (url.match(/[?&]apos-ajax=1/)) {
          hasdynamicPage = true;
        }
  
        if (typeof (data) === 'string') {
          if (!hasdynamicPage) {
            if (data.match(/=/)) {
              data += '&apos-ajax=1';
            } else {
              data += 'apos-ajax=1';
            }
          }
          if (data.match(/(^|&)append=1/)) {
            append = true;
          }
        } else if (typeof (data) === 'object') {
          if (!hasdynamicPage) {
            data['apos-ajax'] = 1;
          }
          if (data.append) {
            append = true;
          }
        }
  
        options = _.extend({
          method: 'GET',
          append: append
        }, options || {});
  
        if (append) {
          options.push = false;
        }
  
        apos.emit('beforeAjax', $old);
  
        $.ajax({
          url: url,
          method: options.method,
          // Presence of a special query parameter to keep this from being cached as the
          // content for a full page
          data: data,
          success: function(html) {
            var url = this.url;
            if (options.push !== false) {
              self.ajaxPushHistory(name, url);
            }
            var $new = $('<div></div>');
            var $appending;
            $new.html(html);
            if (append) {
              // the replacement algorithm changes the scrolling position,
              // which is good for pagination but bad for appending, so make
              // a note of the old scrolling position
              var scrollElement = document.scrollingElement || document.documentElement;
              oldTop = scrollElement.scrollTop;
              $appending = $new.find('[data-dynamic-append]');
              $appending.prepend($appendTo.contents());
            }
            self.replaceUnpreserved($old, $new, { text: true });
            // Note that this may enhance preserved elements twice. If this is
            // a concern you can avoid it by checking for `data-apos-ajax-preserved`
            apos.emit('enhance', $old);
            apos.emit('ajax', $old);
            if (append) {
              $('html, body').scrollTop(oldTop);
              $old.imagesReady(function() {
                // In case one page of content did not actually
                // add enough content to pass the bottom of the screen,
                // check whether infinite scroll should fire again
                self.ajaxInfiniteScrollHandler();
              });
            }

            if(callback) callback();
          },
          error: function(err) {
            self.ajaxError(err);
          }
        });
  
        return false;
  
      };
  
      self.ajaxPopStateHandler = function(event) {
        // jQuery hasn't normalized this one yet, so we need to go to the original for the state
        event = event.originalEvent;
        if (event.state && event.state.dynamicPage) {
          self.ajaxGo(event.state.dynamicPage.name, document.location.href, null, { push: false });
        }
      };
  
      // Push the specified URL to the browser history, associating it with the named data-dynamic-content
      // element in the page. Any apos-ajax query parameter is stripped off so that the history shows
      // full-page URLs rather than ajax refresh URLs. You will not need to call this method yourself. It is
      // called for you by `ajaxGo`.
  
      self.ajaxPushHistory = function(name, url) {
        // We don't want the extra query parameter that prevents IE from caching the ajax response as a regular
        // full page response to be in the history
        url = url.replace('&apos-ajax=1', '');
        url = url.replace('?apos-ajax=1', '?');
        url = url.replace(/\?$/, '');
        // Call pushState if the browser supports it, if not our graceful degradation is that you don't get
        // "back" support, pretty reasonable considering how rare IE9 is now
        if (('history' in window) && ('pushState' in window.history)) {
          if (!self.ajaxStatePushed) {
            // If this is our first push, establish a state object for the
            // original URL, so we can click "back" to undo our first AJAX click
            window.history.replaceState({ 'dynamicPage': { url: window.location.href, name: name } }, $('title').text(), window.location.href);
            self.ajaxStatePushed = true;
          }
          window.history.pushState({ 'dynamicPage': { url: url, name: name } }, $('title').text(), url);
        }
      };
  
     
  
      self.ajaxError = function(err) {
        apos.utils.error(err);
        apos.notify('Unfortunately a server error occurred. Please try again later.', { type: 'error', dismiss: true });
      };

   

    self.replaceUnpreserved = function($old, $new, options) {

        options = options || {};
  
        if (options.text) {
          autoPreserveText();
        }
  
        removeOrphans();
        tagAncestors($old);
        tagAncestors($new);
        cleanout($old);
        restore($old, $new);
  
        function autoPreserveText() {
          var $preserve = $old.find('input[name]:focus,textarea[name]:focus');
          $preserve.each(function() {
            var type = $(this).attr('type');
            // Watch out for input element types that are not actually text
            if (_.includes([ 'checkbox', 'radio', 'submit' ], type)) {
              return;
            }
            var $el = $(this);
            var name = $el.attr('name');
            attr($el, 'preserve', name);
            attr(findName($new, name), 'preserve', name);
          });
        }
  
        // If an element no longer exists in the new markup, then there is nothing to preserve, so don't try
        function removeOrphans() {
          var $alive = find($new, 'preserve');
          var $previous = find($old, 'preserve');
          $previous.each(function() {
            var $el = $(this);
            if (!isVal($alive, 'preserve', attr($el, 'preserve'))) {
              $el.remove();
            }
          });
        }
  
        // Tag ancestors of each preserved element as preserving it, in a way that uniquely tags them for each
        // preserved element, including indicating how many generations removed they are, so that we can find
        // equivalent parent elements between $old and $new
        function tagAncestors($context) {
          // If we don't do this they start doubling up on the second and subsequent refreshes
          find($context, 'preserves').removeAttr('data-apos-ajax-preserves');
          var $preserve = find($context, 'preserve');
          $preserve.each(function() {
            var $el = $(this);
            var level = 0;
            $el.parentsUntil($context).each(function() {
              var $ancestor = $(this);
              addPreserves($ancestor, $el, level);
              level++;
            });
          });
        }
  
        // Scrap all elements of $old that are neither preserved, nor preserving a descendant
        function cleanout($old) {
          $old.contents().each(function() {
            var $node = $(this);
            if (is($node, 'preserves')) {
              cleanout($node);
            } else if (is($node, 'preserve')) {
              // Leave it
            } else {
              $node.remove();
            }
          });
        }
  
        // Copy children of $new to $old except where a preserved element or its ancestor must be respected;
        // flow the new elements around those, after invoking recursively where appropriate
        function restore($old, $new) {
          // We append the next element after this point. If $after is not yet set, we prepend to $new
          // and then set $after to the node just prepended.
          var $after;
          $new.contents().each(function() {
            var $node = $(this);
            var handled = false;
            var $nodeOld;
            if (is($node, 'preserve')) {
              $nodeOld = findVal($old, 'preserve', attr($node, 'preserve'));
              $after = $nodeOld;
              handled = true;
            } else if (is($node, 'preserves')) {
              $nodeOld = findVal($old, 'preserves', attr($node, 'preserves'));
              if ($nodeOld.length) {
                restore($nodeOld, $node);
                $after = $nodeOld;
                handled = true;
              }
            }
            if (!handled) {
              if ($after) {
                $after.after($node);
              } else {
                $old.prepend($node);
              }
              $after = $node;
            }
          });
        }
  
        // Fetch an array of name:ancestorLevel pairs indicating what this element is preserving
        function getPreserves($el) {
          var val = attr($el, 'preserves');
          if (!val) {
            return [];
          }
          return val.split(',');
        }
  
        // Add a new name:ancestorLevel pair for the element specified by $preserve
        function addPreserves($el, $preserve, level) {
          attr($el, 'preserves', getPreserves($el).concat([ attr($preserve, 'preserve') + ':' + level ]).join(','));
        }
  
        // Find prefixed
        function find($el, key) {
          return $el.find('[data-apos-ajax-' + key + ']');
        }
  
        // Find prefixed with specified value
        function findVal($el, key, val) {
          return $el.find('[data-apos-ajax-' + key + '="' + val + '"]');
        }
  
        // .is() prefixed
        function is($el, key) {
          return $el.is('[data-apos-ajax-' + key + ']');
        }
  
        // is() prefixed with specified value
        function isVal($el, key, val) {
          return $el.is('[data-apos-ajax-' + key + '="' + val + '"]');
        }
  
        // .attr() prefixed, getter and setter
        function attr($el, key, val) {
          if (arguments.length === 2) {
            return $el.attr('data-apos-ajax-' + key);
          }
          $el.attr('data-apos-ajax-' + key, val);
        }
  
        // Find by name attribute, unprefixed, for autoprotecting input fields
        function findName($el, name) {
          return $el.find('[name="' + name + '"]');
        }
  
      };
      

      return self;
}