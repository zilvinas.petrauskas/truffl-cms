
var MENU_VISIBLE = false;
var TRANSITIONS_ENABLED = true;
var appearedElements = [];
var ScrollMagicController = undefined;

document.addEventListener('DOMContentLoaded', function () {
    initScrollMagic();
    preparePopupPages();
    createDynamicLinks();
    initNavMenu();
    initMenuForm();
    initFooterForm();
    initSubmenuEvents();
    // initDynamicPageLoad();
    onScrollAppear();
    detectChanges();
    fixIosFullHeight();
});

window.addEventListener('load', function () {
    document.querySelectorAll('body')[0].classList.add('loaded');
    // initPageTransitions();
    initPageTransitions2();
})

globalCssRootRules();
window.addEventListener('resize', function () {
    globalCssRootRules();
});

function isMobile(){
    return (function(a){return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4));})(navigator.userAgent||navigator.vendor||window.opera);
}

function isTablet(){
    const userAgent = navigator.userAgent.toLowerCase();
    return /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);
}

function initScrollMagic(){
    ScrollMagicController = new ScrollMagic.Controller();
    var mobile = isMobile();
    // if(mobile){
    //     // configure iScroll
	// 		var myScroll = new IScroll('body',
    //         {
    //             // don't scroll horizontal
    //             scrollX: false,
    //             // but do scroll vertical
    //             scrollY: true,
    //             // show scrollbars
    //             scrollbars: true,
    //             // deactivating -webkit-transform because pin wouldn't work because of a webkit bug: https://code.google.com/p/chromium/issues/detail?id=20574
    //             // if you dont use pinning, keep "useTransform" set to true, as it is far better in terms of performance.
    //             useTransform: false,
    //             // deativate css-transition to force requestAnimationFrame (implicit with probeType 3)
    //             useTransition: false,
    //             // set to highest probing level to get scroll events even during momentum and bounce
    //             // requires inclusion of iscroll-probe.js
    //             probeType: 3,
    //             // pass through clicks inside scroll container
    //             click: true 
    //         }
    //     );
    //     // overwrite scroll position calculation to use child's offset instead of container's scrollTop();
    //     ScrollMagicController.scrollPos(function () {
    //         return -myScroll.y;
    //     });

    //     // thanks to iScroll 5 we now have a real onScroll event (with some performance drawbacks)
    //     myScroll.on("scroll", function () {
    //         ScrollMagicController.update(true);
    //     });
    // }
}

function fixIosFullHeight(){

    window.addEventListener('load', updateViewHeight);
    window.addEventListener('resize', updateViewHeight);

    function updateViewHeight(){
        // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
        let vh = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    }
}

function detectChanges(){

    /*
        Detect if admin UI is open and then disable all .appear classes
    */

    var state = 0;
    var timer = setInterval(function(){
        check();
    }, 1000);

    function check(){

        // apos-ui apos-modal apos-reorganize-modal apos-separate-trash
        // apos-ui apos-modal project-manager apos-ui-modal-no-sidebar apos-manager apos-manager-modal--has-no-choices
        // apos-ui apos-modal apos-modal-slideable
        if( state === 0 && $('.apos-ui.apos-modal').is(':visible')){
            state = 1;
            console.log('admin panel open');
        }
        if(state === 1){
            $('body').addClass('disable-reveals force-hide-intro'); // qqli
            state = 2;
            clearInterval(timer);
            console.log('reveals disabled');
        }else if(state === 2){
            console.log('timer should be disabled already!');
        }
    }


    window.addEventListener('widget:create', function(e){

        var params = e.detail;

        if($('body').hasClass('disable-reveals')){
            if(params.type == 'video'){
                // widget:video
                window.dispatchEvent(new CustomEvent('widget:video', {
                    detail: e.detail,
                }));
            }else{
                window.dispatchEvent(new CustomEvent('widget:any', {
                    detail: e.detail,
                }));
            }
        }


    });


}

function globalCssRootRules() {

    var fixRatio = 1;
    var aspectRatio = window.innerWidth / window.innerHeight;

    if (aspectRatio > 2.13) { // 1600 / 750

        if (aspectRatio > 3) {
            fixRatio = 0.6;
        } else if (aspectRatio > 2.8) {
            fixRatio = 0.65;
        } else if (aspectRatio > 2.6) {
            fixRatio = 0.7;
        } else if (aspectRatio > 2.5) {
            fixRatio = 0.75;
        } else if (aspectRatio > 2.4) {
            fixRatio = 0.8;
        } else if (aspectRatio > 2) {
            fixRatio = 0.85;
        } else if (aspectRatio > 16 / 9) {
            fixRatio = 0.9;
        }
    }

    document.documentElement.style.setProperty('--fixed-ratio', fixRatio);
}

function preparePopupPages() {

    new FullPagePopup({
        trigger: document.querySelectorAll('#hire-us-button')[0],
        container: document.querySelectorAll('#hire-us-popup-page')[0],
        jQueryForm: {
            form: $('#hire-us-form'),
            rules: {
                "first-name": {
                    required: true,
                    minlength: 2,
                },
                "last-name": {
                    required: true,
                    minlength: 2,
                },
                "email": {
                    required: true,
                    email: true
                },
                "subject": {
                    required: true,
                    minlength: 5,
                },
            },
        }
    });

    // contact us
    new FullPagePopup({
        trigger: document.querySelectorAll('#contact-us-button')[0],
        container: document.querySelectorAll('#contact-us-popup-page')[0],
        jQueryForm: {
            form: $('#contact-us-form'),
            rules: {
                "first-name": {
                    required: true,
                    minlength: 2,
                },
                "last-name": {
                    required: true,
                    minlength: 2,
                },
                "email": {
                    required: true,
                    email: true
                },
                "subject": {
                    required: true,
                    minlength: 5,
                },
                "message": {
                    required: true,
                    minlength: 10,
                },
            }
        }
    });

}

function createDynamicLinks() {

    var elements = document.querySelectorAll('[data-create]');
    if (elements.length > 0) {
        for (let i = 0; i < elements.length; i++) {
            let attr = elements[i].getAttribute('data-create');
            let text = elements[i].querySelector('p');
            elements[i].setAttribute('href', attr + '' + text.textContent);
        }
    }

}

function initNavMenu() {

    var $burger = $('#burger');
    var $menu = $('#menu');
    var menuOpen = false;
    var timeout;

    $burger.on('click', function () {
        toggleMenuNew();
    });

    window.addEventListener('menu:hide', function () {
        toggleMenuNew();
    });


    function toggleMenuNew() {

        MENU_VISIBLE = menuOpen = !menuOpen;

        // while menu is open - disable hover effect
        if (menuOpen) {
            clearTimeout(timeout);
            $burger.addClass('busy');
        } else {
            timeout = setTimeout(function () {
                $burger.removeClass('busy');
            }, 500);
        }

        $menu.toggleClass('menu-expanded');
        $burger.toggleClass('active');
    }
}

function initSubmenuEvents() {

    var busy = false;

    $('.expand-submenu').on('click', function () {

        if (busy) return;

        busy = true;

        var elem = $(this);
        var parent = elem.parent();
        var child = elem.children('.plus-and-minus');
        var submenu = parent.children('.submenu');

        var prevHeight = 0;
        var animationHeight = 0;

        if (!parent.hasClass('expand')) {
            submenu.children('.link-with-underline').each(function () {
                animationHeight += parseInt($(this)[0].scrollHeight);
            });
        }

        console.log('animate', animationHeight);

        animate(submenu, prevHeight, animationHeight, function () {
            busy = false;
        });

        parent.toggleClass('expand');
        child.toggleClass('open');

    });

    function animate(elem, height0, height1, callback) {

        // TweenMax.fromTo(elem, 0.5, 
        //     {
        //         height: height0,
        //     },{
        //         height: height1,
        //         onComplete: function () {
        //             prevHeight = height1;
        //             if (callback) callback();
        //         }
        //     })
        //     .play();

        TweenMax.to(elem, 0.5,
            {
                height: height1,
                onComplete: function () {
                    prevHeight = height1;
                    if (callback) callback();
                }
            })
            .play();

    }


}

function initFooterForm(){

    var form = $('#footer-newsletters');
    var errorContainer = '.error-container';

    var validator = form.validate({
        rules: {
            // name: {
            //     required: true,
            //     minlength: 2,
            // },
            email: {
                required: true,
                email: true,
                minlength: 5
            }
        },
        errorClass: "invalid",
        focusInvalid: true,
        submitHandler: function (f) { }, // called manually after checkForm()
        highlight: function (element, errorClass, validClass) {
            var parent = $(element).parents(errorContainer);
            if (parent.length === 0) parent = $(element);
            parent.addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            var parent = $(element).parents(errorContainer);
            if (parent.length === 0) parent = $(element);
            parent.removeClass(errorClass);
        },
        errorPlacement: function (error, element) { } // prevent creating html elements for error, only highlight them
    })

    $('#footer-newsletters-button').on('click', function () {
        var isValid = validator.form();
        console.log('footer form valid?', isValid);
        if (isValid) submitForm();
    });

    function setName(){
        var email = form.find('[name="email"]').val();
        form.find('[name="name"]').val(email.split('@')[0]);
    }

    function submitForm() {

        setName(); // footer form missing name, so create it from email
        var action = form[0].dataset.action;
        var input = form.serialize();

        console.log('-- Serialize input', input);
        console.log('-- sending form to ', action);

        $.post(action, input, function (result) {
            if (result.status === 'ok') {
                showThankYou();
            } else {
                console.log('--- CHECK RESPONSE', result);
            }
            return false;
        });
        return false;
    }

    function showThankYou() {
        $('#footer .subscribe').addClass('show-thank-you');
    }

}

function initMenuForm() {

    var form = $('#menu-newsletters');
    var errorContainer = '.error-container';

    var validator = form.validate({
        rules: {
            name: {
                required: true,
                minlength: 2,
            },
            email: {
                required: true,
                email: true,
                minlength: 5
            }
        },
        errorClass: "invalid",
        focusInvalid: true,
        submitHandler: function (f) { }, // called manually after checkForm()
        highlight: function (element, errorClass, validClass) {
            var parent = $(element).parents(errorContainer);
            if (parent.length === 0) parent = $(element);
            parent.addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            var parent = $(element).parents(errorContainer);
            if (parent.length === 0) parent = $(element);
            parent.removeClass(errorClass);
        },
        errorPlacement: function (error, element) { } // prevent creating html elements for error, only highlight them
    })

    $('#menu-newsletters-button').on('click', function () {
        var isValid = validator.form();
        console.log('form valid?', isValid);
        if (isValid) submitForm();
    })

    function submitForm() {

        var action = form[0].dataset.action;
        var input = form.serialize();

        console.log('-- Serialize input', input);
        console.log('-- sending form to ', action);

        $.post(action, input, function (result) {
            if (result.status === 'ok') {
                showThankYou();
            } else {
                console.log('--- CHECK RESPONSE', result);
            }
            return false;
        });
        return false;
    }

    function showThankYou() {
        $('#menu-stay-updated').addClass('show-thank-you');
    }

}

function isLocalhost() {
    console.log('localhost?', location.hostname)
    return location.hostname === "localhost" || location.hostname === "127.0.0.1";
}

function animatedPartnersLogos(config) {

    var animationConfig = {
        // speed: 1250, // css
        delay: 3300, // css animation speed + delay
        firstDelay: 2500,
    }

    hideAnimationsForAdmin();

    /*
    1. get logos
    2. create extra html required for animations
    3. switch logos around
    */
    //    var elements = document.querySelectorAll('.animated-partners-logos .logo-animated');
    var elements = config.elements;
    var data = [];
    var parent, found, overlay1, overlay2, src;
    var firstAnimation = true;

    for (let i = 0; i < elements.length; i++) {

        found = false;
        parent = undefined;
        while (!found) {
            parent = parent === undefined ? elements[i].parentElement : parent.parentElement;
            if (parent.classList.contains('quarter') || parent.classList.contains('half') || parent.classList.contains('squared-image-wrapper')) {
                found = true;
            }
        }

        // overlays
        src = elements[i].getAttribute('src');

        overlay1 = createOverlay(src, { alt: elements[i].getAttribute('alt') });
        overlay1.classList.add('in');

        overlay2 = createOverlay(src, { alt: elements[i].getAttribute('alt') });

        parent.appendChild(overlay1);
        parent.appendChild(overlay2);

        data.push({
            index: i,
            src: src,
            image: elements[i],
            wrapper: parent,
            overlays: [
                overlay1,
                overlay2
            ], // 2 overlays
            onTop: 0,
        });
    }

    // console.log('ORIG DATA', data);

    // animate7
    var cont;
    var startAt = 1;
    var times = 0;
    doIt();

    function doIt() {

        var delay = animationConfig.delay;
        if(firstAnimation){
            firstAnimation = false;
            delay = animationConfig.firstDelay;
        }

        setTimeout(function () {
            cont = animate();
            if (cont) doIt();
        }, animationConfig.delay);
    }

    function animate() {

        var src;
        var anim = {
            counter: 0,
            switch: 1,
            side: 'anim-left', // 'anim-left', 'anim-top',
        }

        // push last element in front
        // var first = data[0];
        // data.shift();        
        // data.push(first);
        var next;
        var pos;
        var current = startAt;
        for (let i = 0; i < data.length; i++) {

            anim.counter++;
            if (anim.counter >= anim.switch) {
                anim.side = anim.side == 'anim-left' ? 'anim-top' : 'anim-left';
                anim.counter = 0;
            }

            // pos = getPos(current, i, data.length-1);
            next = data[i];
            animateSingle(data[current], data[current].onTop, next.src, anim.side);

            // console.log('start', startAt, pos, next.index);

            current += 1;
            if (current >= data.length) current = 0;
        }

        startAt += 1;
        if (startAt >= data.length) startAt = 0;

        // console.log('start?', startAt);

        times++;
        // if(times > 0) return false;

        return true;
    }

    function getPos(start, index, max) {
        var pos = start + index;
        if (pos > max) {
            console.log('too big', pos, max);
            while (pos > max) pos -= max;
            console.log('-- fixed', pos);
        }
        return pos;
    }

    function animateSingle(current, primary, src, animationClass) {

        var secondary = primary === 0 ? 1 : 0;

        // secondary - change src, increase zIndex
        // priamry reduce zIndex

        if (!current.wrapper.classList.contains(animationClass)) {
            current.wrapper.classList.remove('anim-top', 'anim-left');
            current.wrapper.classList.add(animationClass);
        }

        current.overlays[secondary].setAttribute('src', src);

        current.overlays[primary].classList.remove('ontop');
        current.overlays[secondary].classList.add('ontop');
        current.overlays[secondary].classList.add('in');

        // wait till animation is complete. And...
        current.overlays[secondary].addEventListener('transitionend', onTransitionEnd);

        function onTransitionEnd() {
            current.overlays[primary].classList.remove('in');
            current.overlays[secondary].removeEventListener('transitionend', onTransitionEnd);
            current.onTop = current.onTop === 0 ? 1 : 0;
        }
    }


    function createOverlay(src, options) {
        var overlay = document.createElement("img");
        if (Object.keys(options).length > 0) {
            for (let key in options) {
                overlay.setAttribute(key, options[key]);
            }
        }
        overlay.setAttribute('src', src);
        overlay.classList.add('fit', 'clone');
        return overlay;
    }


    function hideAnimationsForAdmin() {

        var className = 'stop-animations';
        var key = 20; // capslock

        window.addEventListener('keyup', function (event) {
            // console.log('event', event.keyCode);
            if (event.keyCode === key) {
                document.body.classList.toggle(className);
            }
        });

    }
}

function waitUntilLoaded(callback) {

    // TODO - make sure top of page is loaded (images, videos, etc)
    setTimeout(function () {
        if (callback) callback();
    }, 500);

}


function initPageTransitions2(){

    var spinner = $('#spinner');
    var curtains = $('#curtains');
 
    initIntro();
    initOutro();

    function initIntro(callback){

        spinner.addClass('visible');
        
        triggerTransitionStatusEvent('transition:step1');

        setTimeout(function(){
            curtains.addClass('open');
            spinner.removeClass('visible');
            triggerTransitionStatusEvent('transition:step2');
        }, 1000);

        setTimeout(function(){
            curtains.removeClass('visible');
            triggerTransitionStatusEvent('transition:end');
            if(callback) callback();
        }, 2000);


       
        // window.addEventListener('intro:hide', function(){
            // $('body').addClass('force-hide-intro');
            // curtains.addClass('open');
            // spinner.removeClass('visible');
            // setTimeout(function(){
            //     curtains.removeClass('visible');
            // }, 1000);
        // });
    }

    function initOutro() {

        var delay = 1000;

        $('a:not(.dynamic):not(.external)[href]').on('click', function (event) {

            event.preventDefault();
            play();
            redirect($(this).attr('href'), delay);

        });

        function play() {
            curtains.addClass('visible close');
        }
    }

    function redirect(url, delay) {
        console.log('delayed redirect to', url);
        setTimeout(function () {
            window.location = url;
        }, delay);
    }

}

function triggerTransitionStatusEvent(name){
    window.dispatchEvent(new CustomEvent(name));
}

function initPageTransitions() {

    var isHomePage = $('.home').length > 0;
    var introSlides = $('#pageIntro');
    var spinner = $('#spinner');
    var bubblyWrapper = $('.bubbly-canvas-wrapper');

    if (!TRANSITIONS_ENABLED) {

        introSlides.hide();
        bubblyWrapper.hide();

    } else {
        initIntro(function () {
            initOutro();
        });
    }



    function initIntro(callback) {

        var bubblyBg = animatetBubbles();
        var introCompletedDelay = 1000;
        var initialDelay = 100;

        if (isHomePage) {
            introCompletedDelay = 3100;
            initialDelay = 0;
        }

        if (!isHomePage) {
            spinner.show();
        }

        setTimeout(function () {
            

            if (isHomePage) {

                console.log('2A')
                playIntroHeyAnim();
                // introSlides.addClass('play slow');
                introSlides.addClass('hide');
                setTimeout(function () {
                    bubblyBg.run(
                        function(){
                            triggerTransitionStatusEvent('transition:end');
                        }, 
                        function(){
                            triggerTransitionStatusEvent('transition:step2');
                        },
                        function(){
                            triggerTransitionStatusEvent('transition:step1');
                        }
                    );
                }, 1100);

            } else {

                setTimeout(function () {
                    spinner.hide(500);
                    bubblyBg.run(
                        function(){
                            triggerTransitionStatusEvent('transition:end');
                        }, 
                        function(){
                            triggerTransitionStatusEvent('transition:step2');
                        },
                        function(){
                            triggerTransitionStatusEvent('transition:step1');
                        }
                    );
                }, 1000);

                introSlides.addClass('hide');
                // introSlides.addClass('play');
            }

            window.addEventListener('transition:end', function(){
                $('body').addClass('intro-complete');
                if (callback) callback();
            })

        }, initialDelay);


    }

   

    function initOutro() {

        var delay = 1000;
        var outroSlides = $('#pageOutro');

        $('a:not(.dynamic)[href]').on('click', function (event) {

            event.preventDefault();
            play();
            redirect($(this).attr('href'), delay);

        });

        function play() {

            outroSlides.addClass('play');

            // setTimeout(function(){
            //     outroSlides.removeClass('play');
            // }, 4000);
        }
    }

    function redirect(url, delay) {
        console.log('delayed redirect to', url);
        setTimeout(function () {
            window.location = url;
        }, delay);
    }

    function playIntroHeyAnim() {
        $('.hello').addClass('play');
    }
}

function animatetBubbles() {

    var transitionBg = "#1d1d1d";
    // var transitionBg = "#000";
    var container = $('.bubbly-canvas-wrapper');
    var canvas = $('#bubblyCanvas');
    var ctx = canvas[0].getContext('2d');

    var w = $(window).width();
    var h = $(window).height();

    var config = getConfig();
    config.grid.data = createGridData();

    var circles = {};
    var startAngle = 0;
    var endAngle = Math.PI * 2;

    var cycles = {
        current: 0,
        max: 100,
    }



    resizeCanvas();

    return {
        run: function (finalCallback, callbackStep2, callbackStep1) {

            console.log('--- STEP #1');
            if(callbackStep1) callbackStep1();

            draw(finalCallback, callbackStep2, callbackStep1);
            container.css('background', 'transparent');
        }
    }

    function getConfig() {

        var config = {
            circles: {
                total: 12,
                minSize: 5,
                maxSize: 20,
            },
            grid: {
                cols: 4, // cols * rows = circles.total
                rows: 3,
            }
        }

        var configMobile = {
            circles: {
                total: 6,
                minSize: 10,
                maxSize: 25,
            },
            grid: {
                cols: 2,
                rows: 3,
            }
        }

        if (window.innerWidth < 800) {
            console.log('MOBILE BUBBLES CONFIG');
            return configMobile;
        }

        return config;
    }

    function onComplete() {
        container.css('transition', '0.5s all').css('opacity', 0);
        setTimeout(function () {
            container.css('visibility', 'hidden');
        }, 500);
    }

    function draw(finalCallback, callbackStep2, callbackStep1) {

        ctx.clearRect(0, 0, w, h);

        // color in the background
        ctx.fillStyle = transitionBg;
        ctx.fillRect(0, 0, w, h);

        ctx.globalCompositeOperation = "xor";

        for (var i = 1; i <= config.circles.total; i++) {
            drawCircle(i, i === 1, i === config.circles.total);
        }


        if (cycles.current === cycles.max) {

            onComplete();
            console.log('--- STEP FINAL');
            if (finalCallback) finalCallback();

        } else if (cycles.current < cycles.max) {

            // step1 callback
            if(cycles.current === 1){
                
            }

            // step2 callback
            if(parseInt(cycles.max / 2) === cycles.current){
                console.log('--- STEP #2');
                if(callbackStep2) callbackStep2();
            }

            requestAnimationFrame(function () {
                cycles.current += 1;
                draw(finalCallback, callbackStep2, callbackStep1);
            })

        }
    }

    function drawCircle(i, first, last) {

        var circle;
        if (circles.hasOwnProperty(i)) {
            circle = circles[i];

            if (!first && !last && !circle.active)
                circle.active = getRandomInt(0, 2) > 0;

            if (circle.active)
                circle.radius += circle.increase; //getRandomInt(1, i);

        } else {

            circle = {
                pos: generatePosition(i),
                pos2: getRandomInt(0, 1) === 1 ? generatePosition(generateDestination(i)) : null, // animated position
                // pos2: generatePosition(generateDestination(i)), // animated position
                radius: getRandomInt(config.circles.minSize, config.circles.maxSize),
                active: getRandomInt(0, 1) === 1,
                increase: getCircleGrowthNumber(i),
            }
            circles[i] = circle;
        }

        if (first) ctx.beginPath();

        if (cycles.current >= 1 && circle.pos2 !== null) {
            circles[i].pos = circle.pos = updateCirclePosition(circle.pos, circle.pos2, cycles);
        }

        if (circle.active) {
            ctx.moveTo(circle.pos.x, circle.pos.y);
            ctx.arc(circle.pos.x, circle.pos.y, circle.radius, startAngle, endAngle, false);
        }

        if (last) {
            ctx.closePath();
            ctx.fillStyle = "#006699";
            ctx.fill();
        }
    }

    function generateDestination(i) {

        /*
            idea is to have it move farer
            so index of 1 should move to 7,8,9
            while 9 to 1,2,3
            and so on...
        */

        var from, to = 0;
        var r = i / config.circles.total; // [0,1]
        if (r < 0.33) {
            from = parseInt(config.circles.total * 0.67);
            to = parseInt(config.circles.total);
        } else if (r > 0.67) {
            from = parseInt(1);
            to = parseInt(config.circles.total * 0.33);
        } else {
            from = parseInt(1, config.circles.total);
            to = parseInt(1, config.circles.total);
        }

        return getRandomInt(from, to);
    }

    /*
        pos - starting position
        pos2 - end position
        c - cycles{ current, max }
    */
    function updateCirclePosition(pos, pos2, c) {

        var step = {
            x: (pos2.x - pos.x) * easing(c.current / 2 / c.max),
            y: (pos2.y - pos.y) * easing(c.current / 2 / c.max)
        }

        var x = parseInt(pos.x + step.x);
        var y = parseInt(pos.y + step.y);

        if (x < 0) x = 0;
        if (y < 0) y = 0;

        return {
            x: x,
            y: y
        }

    }

    function easing(x) {
        return -(Math.cos(Math.PI * x) - 1) / 2;
        // return 1 - Math.pow(1 - x, 3);
        // return x*x;
    }

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
    }

    /*
      Need a smarter way of generating circles position so it fills screen faster
      So basically depending on "i" generate a grid and randomize its position a bit
    */
    function generatePosition(i) {

        var data = config.grid.data[i - 1];
        return {
            x: getRandomInt(data.x.min, data.x.max),
            y: getRandomInt(data.y.min, data.y.max),
        };

        // for test
        // return {
        // x : parseInt( (data.x.min + data.x.max) / 2),
        // y : parseInt( (data.y.min + data.y.max) / 2),
        // }
    }

    // circles in the middle should grow faster
    function getCircleGrowthNumber(index) {

        var growthData = [
            [5, 8], // normal
            [9, 12], // increased
            [12, 20], // center piece
        ]

        // compare to grid, if center piece - magnify by x3, if side piece, magnify by 0.5
        var growthRange = growthData[0];

        // find circle position in the grid
        var x = y = 1;
        for (var i = 1; i <= config.circles.total; i++) {
            if (i > 1) {
                if (x >= config.grid.cols) y += 1;
                x = x >= config.grid.cols ? 1 : x + 1;
            }
            if (i === index) {
                break;
            }
        }

        // magnify faster if circle is clsoer to center
        if (x / 2 === config.grid.cols &&
            y / 2 === config.grid.rows) {
            growthRange = growthData[2];

            // if piece aruond center
        } else if (x / 2 - 1 < config.grid.cols / 2 &&
            x / 2 + 1 > config.grid.cols / 2 &&
            y / 2 - 1 < config.grid.rows / 2 &&
            y / 2 + 1 > config.grid.rows / 2
        ) {
            growthRange = growthData[1];
        }

        var growth = getRandomInt(growthRange[0], growthRange[1]);
        return growth;
    }

    function createGridData() {

        var data = [];
        var gridW = window.innerWidth / config.grid.rows;
        var gridH = window.innerHeight / config.grid.cols;
        var x = y = 1;

        for (var i = 1; i <= config.circles.total; i++) {

            if (i > 1) {
                if (x >= config.grid.cols) y += 1;
                x = x >= config.grid.cols ? 1 : x + 1;
            }

            data.push({
                x: {
                    min: parseInt(gridW * (x - 1)),
                    max: parseInt(gridW * x) - 1,
                },
                y: {
                    min: parseInt(gridH * y), // [0] -> 0, gridH * y+1
                    max: parseInt(gridH * (y + 1)),
                }
            });
        }

        console.log('DATA?', data);

        return data;
    }



    function resizeCanvas() {

        // canvas.width = window.innerWidth;
        // canvas.height = window.innerHeight;
        canvas[0].width = w;
        canvas[0].height = h;
        canvas.css('width', `${w}px`).css('height', `${h}px`);

        console.log('resize canvas', w, h);
    }

}

function initDynamicPageLoad() {

    var options = {};
    var dynamic = dynamicPageFlow(apos, options);
    // var pageContent =  $('.page-content');
    var pageContent = $('.wtf');

    $('[data-ajax]').each(function (index, element) {

        var $elem = $(element);

        $elem.attr('data-href', $elem.attr('href'));
        $elem.attr('href', 'javascript:;');
        $elem.attr('data-check', getDynamicHref($elem));

        $elem.on('click', function (event) {

            event.preventDefault();

            var sendHref = getDynamicHref($(element));

            triggerMenuClose();

            $('.page').remove();

            dynamic.ajaxClickHandler(event, sendHref, pageContent, function () {
                setTimeout(function () {
                    // pageContent.show('slow');
                }, 100);
            });
        })

    });

    function triggerMenuClose() {
        if (MENU_VISIBLE) {
            window.dispatchEvent(new CustomEvent('menu:hide', {
                detail: {}
            }))
        }

    }


    function getDynamicHref(elem) {

        var href = elem.attr('data-href');
        var splitHref = href.split('/');
        var shortLink = splitHref[splitHref.length - 1];
        // var href = elem.attr('data-href').split('/');

        // var shortLink = href[href.length - 1];
        // $(".page-content").attr('data-apos-ajax-context', shortLink);

        // var sendHref = window.location.origin + window.location.pathname + '/' + shortLink;
        // var sendHref = window.location.origin + window.location.pathname + shortLink;
        var sendHref = window.location.origin + href;
        console.log('-sendHref', sendHref);

        return sendHref;
    }
}

function dynamicPageFlow(apos) {


    var self = this;

    self.ajaxClickHandler = function (event, _href, pageContent, callback) {
        var $anchor = $(this);

        var href = _href;
        var ajaxContext = self.ajaxContextOf($anchor);
        self.ajaxGo(ajaxContext, href, null, { container: pageContent }, callback);
        return false;
    };

    self.ajaxContextOf = function ($el) {
        //   var title = $('[data-page-title]').text();
        var title = $('[data-apos-ajax-context]').text();
        console.log('GOT TITLE?', title);
        return title || 'test';
        //   return $('#casePageContent').attr('data-dynamic-content');
    };

    // TODO - load styles, javascripts, clean up (apos)
    self.ajaxGo = function (name, url, data, options, callback) {

        data = data || {};

        if(! options.method ) options.method = 'GET';
        data['apos-ajax'] = 1;
        
        $.ajax({
            url: url,
            method: options.method,
            // Presence of a special query parameter to keep this from being cached as the
            // content for a full page
            data: data,
            success: function (html) {
                var url = this.url;
                if (options.push !== false) {
                    self.ajaxPushHistory(name, url);
                }

                options.container.html(html);
                options.container.greenify();

                if (callback) callback();
            },
            error: function (err) {
                self.ajaxError(err);
            }
        });

        return false;

    }

    self.ajaxGo2 = function (name, url, data, options, callback) {
        data = data || {};
        var $old = $('[data-dynamic-content="' + name + '"]');
        // var $old = $('[data-dynamic-content="page"]');
        // var container = options.container;
        // var $old = $('[data-dynamic-content="page"]');
        var $appendTo = $old.find('[data-dynamic-append]');
        var append = false;
        var hasdynamicPage = false;
        var oldTop;

        if (url.match(/[?&]append=1/)) {
            append = true;
        }

        if (url.match(/[?&]apos-ajax=1/)) {
            hasdynamicPage = true;
        }

        if (typeof (data) === 'string') {
            if (!hasdynamicPage) {
                if (data.match(/=/)) {
                    data += '&apos-ajax=1';
                  
                } else {
                    data += 'apos-ajax=1';
                   
                }
            }
            if (data.match(/(^|&)append=1/)) {
                append = true;
            }
        } else if (typeof (data) === 'object') {
            if (!hasdynamicPage) {
                data['apos-ajax'] = 1;
            }
            if (data.append) {
                append = true;
            }
        }

        options = _.extend({
            method: 'GET',
            append: append
        }, options || {});

        if (append) {
            options.push = false;
        }

        apos.emit('beforeAjax', $old);

        $.ajax({
            url: url,
            method: options.method,
            // Presence of a special query parameter to keep this from being cached as the
            // content for a full page
            data: data,
            success: function (html) {
                var url = this.url;
                if (options.push !== false) {
                    self.ajaxPushHistory(name, url);
                }
                // var $new = $('<div></div>');
                var $new = options.container;
                // var $appending;

                $new.html(html);


                // if (append) {
                //   // the replacement algorithm changes the scrolling position,
                //   // which is good for pagination but bad for appending, so make
                //   // a note of the old scrolling position
                //   var scrollElement = document.scrollingElement || document.documentElement;
                //   oldTop = scrollElement.scrollTop;
                //   $appending = $new.find('[data-dynamic-append]');
                //   $appending.prepend($appendTo.contents());
                // }

                self.replaceUnpreserved($old, $new, { text: true });

                // Note that this may enhance preserved elements twice. If this is
                // a concern you can avoid it by checking for `data-apos-ajax-preserved`

                apos.emit('enhance', $old);
                apos.emit('ajax', $old);

                if (append) {
                    $('html, body').scrollTop(oldTop);
                    $old.imagesReady(function () {
                        // In case one page of content did not actually
                        // add enough content to pass the bottom of the screen,
                        // check whether infinite scroll should fire again
                        self.ajaxInfiniteScrollHandler();
                    });
                }

                if (callback) callback();
            },
            error: function (err) {
                self.ajaxError(err);
            }
        });

        return false;

    };

    self.ajaxPopStateHandler = function (event) {
        // jQuery hasn't normalized this one yet, so we need to go to the original for the state
        event = event.originalEvent;
        if (event.state && event.state.dynamicPage) {
            self.ajaxGo(event.state.dynamicPage.name, document.location.href, null, { push: false });
        }
    };

    // Push the specified URL to the browser history, associating it with the named data-dynamic-content
    // element in the page. Any apos-ajax query parameter is stripped off so that the history shows
    // full-page URLs rather than ajax refresh URLs. You will not need to call this method yourself. It is
    // called for you by `ajaxGo`.

    self.ajaxPushHistory = function (name, url) {
        // We don't want the extra query parameter that prevents IE from caching the ajax response as a regular
        // full page response to be in the history
        url = url.replace('&apos-ajax=1', '');
        url = url.replace('?apos-ajax=1', '?');
        url = url.replace(/\?$/, '');
        // Call pushState if the browser supports it, if not our graceful degradation is that you don't get
        // "back" support, pretty reasonable considering how rare IE9 is now
        if (('history' in window) && ('pushState' in window.history)) {
            if (!self.ajaxStatePushed) {
                // If this is our first push, establish a state object for the
                // original URL, so we can click "back" to undo our first AJAX click
                window.history.replaceState({ 'dynamicPage': { url: window.location.href, name: name } }, $('title').text(), window.location.href);
                self.ajaxStatePushed = true;
            }
            window.history.pushState({ 'dynamicPage': { url: url, name: name } }, $('title').text(), url);
        }
    };



    self.ajaxError = function (err) {
        apos.utils.error(err);
        apos.notify('Unfortunately a server error occurred. Please try again later.', { type: 'error', dismiss: true });
    };



    self.replaceUnpreserved = function ($old, $new, options) {

        options = options || {};

        if (options.text) {
            autoPreserveText();
        }

        removeOrphans();
        tagAncestors($old);
        tagAncestors($new);
        cleanout($old);
        restore($old, $new);

        function autoPreserveText() {
            var $preserve = $old.find('input[name]:focus,textarea[name]:focus');
            $preserve.each(function () {
                var type = $(this).attr('type');
                // Watch out for input element types that are not actually text
                if (_.includes(['checkbox', 'radio', 'submit'], type)) {
                    return;
                }
                var $el = $(this);
                var name = $el.attr('name');
                attr($el, 'preserve', name);
                attr(findName($new, name), 'preserve', name);
            });
        }

        // If an element no longer exists in the new markup, then there is nothing to preserve, so don't try
        function removeOrphans() {
            var $alive = find($new, 'preserve');
            var $previous = find($old, 'preserve');
            $previous.each(function () {
                var $el = $(this);
                if (!isVal($alive, 'preserve', attr($el, 'preserve'))) {
                    $el.remove();
                }
            });
        }

        // Tag ancestors of each preserved element as preserving it, in a way that uniquely tags them for each
        // preserved element, including indicating how many generations removed they are, so that we can find
        // equivalent parent elements between $old and $new
        function tagAncestors($context) {
            // If we don't do this they start doubling up on the second and subsequent refreshes
            find($context, 'preserves').removeAttr('data-apos-ajax-preserves');
            var $preserve = find($context, 'preserve');
            $preserve.each(function () {
                var $el = $(this);
                var level = 0;
                $el.parentsUntil($context).each(function () {
                    var $ancestor = $(this);
                    addPreserves($ancestor, $el, level);
                    level++;
                });
            });
        }

        // Scrap all elements of $old that are neither preserved, nor preserving a descendant
        function cleanout($old) {
            $old.contents().each(function () {
                var $node = $(this);
                if (is($node, 'preserves')) {
                    cleanout($node);
                } else if (is($node, 'preserve')) {
                    // Leave it
                } else {
                    $node.remove();
                }
            });
        }

        // Copy children of $new to $old except where a preserved element or its ancestor must be respected;
        // flow the new elements around those, after invoking recursively where appropriate
        function restore($old, $new) {
            // We append the next element after this point. If $after is not yet set, we prepend to $new
            // and then set $after to the node just prepended.
            var $after;
            $new.contents().each(function () {
                var $node = $(this);
                var handled = false;
                var $nodeOld;
                if (is($node, 'preserve')) {
                    $nodeOld = findVal($old, 'preserve', attr($node, 'preserve'));
                    $after = $nodeOld;
                    handled = true;
                } else if (is($node, 'preserves')) {
                    $nodeOld = findVal($old, 'preserves', attr($node, 'preserves'));
                    if ($nodeOld.length) {
                        restore($nodeOld, $node);
                        $after = $nodeOld;
                        handled = true;
                    }
                }
                if (!handled) {
                    if ($after) {
                        $after.after($node);
                    } else {
                        $old.prepend($node);
                    }
                    $after = $node;
                }
            });
        }

        // Fetch an array of name:ancestorLevel pairs indicating what this element is preserving
        function getPreserves($el) {
            var val = attr($el, 'preserves');
            if (!val) {
                return [];
            }
            return val.split(',');
        }

        // Add a new name:ancestorLevel pair for the element specified by $preserve
        function addPreserves($el, $preserve, level) {
            attr($el, 'preserves', getPreserves($el).concat([attr($preserve, 'preserve') + ':' + level]).join(','));
        }

        // Find prefixed
        function find($el, key) {
            return $el.find('[data-apos-ajax-' + key + ']');
        }

        // Find prefixed with specified value
        function findVal($el, key, val) {
            return $el.find('[data-apos-ajax-' + key + '="' + val + '"]');
        }

        // .is() prefixed
        function is($el, key) {
            return $el.is('[data-apos-ajax-' + key + ']');
        }

        // is() prefixed with specified value
        function isVal($el, key, val) {
            return $el.is('[data-apos-ajax-' + key + '="' + val + '"]');
        }

        // .attr() prefixed, getter and setter
        function attr($el, key, val) {
            if (arguments.length === 2) {
                return $el.attr('data-apos-ajax-' + key);
            }
            $el.attr('data-apos-ajax-' + key, val);
        }

        // Find by name attribute, unprefixed, for autoprotecting input fields
        function findName($el, name) {
            return $el.find('[name="' + name + '"]');
        }

    };


    return self;


}

function onScrollAppear(){

    var initialDelay = 400;
    // var controller = new ScrollMagic.Controller();

    initEarlyAppearingElements();

    if(TRANSITIONS_ENABLED){
        waitForTransitionEnd(function(){
            initAppearances();
        })
    }else{
        setTimeout(initAppearances, initialDelay);
    }

    function initAppearances(){

        initAppearingElements();
  
        if( $('.home').length > 0){
        
        }
    }

    
    function waitForTransitionEnd(callback){
        window.addEventListener('transition:end', function(){
            callback();
        });
    }

    function initEarlyAppearingElements(){

        var elements = $('.appear.trigger-early');
        if(elements.length === 0)return;

        window.addEventListener('transition:step2', function(){
            elements.each(function(index, elem){
                $(elem).addClass('run');
            });
        });

    }

    function initAppearingElements(){

        var elements = $('.appear:not(.trigger-early)');
        if(elements.length === 0)return;

        elements.each(function(index, elem){
            if(elem.classList.contains('appear-video-play')){
                createVideoScene(elem);
            }else{
                createScene(elem);
            }
           
        });

        // createVideoScene
        // custom event
        // window.addEventListener('video:play', function(e){
        //     console.log('VIDEO:PLAY', e.detail);
        //     var elem = e.detail.elem;
        //     createVideoScene(elem);
        // });
        window.addEventListener('widget:video', function(e){
            createVideoScene(e.detail.element);
        })
        window.addEventListener('widget:any', function(e){
            createScene(e.detail.element);
        })


       function createScene(elem){
            var scene = new ScrollMagic.Scene({
                triggerElement: elem,
                triggerHook: 0.9, // 0.9 - show, when scrolled 10% into view
                offset: 50, // move trigger to center of element
                reverse: false // only do once
            })
            .setClassToggle(elem, "run") // add class toggle
            // .addIndicators() // add indicators (requires plugin)
            .on("start", function(e){
                appearedElements.push(elem);
            })
            .addTo(ScrollMagicController);
       }

       

       function createVideoScene(elem){

            var videoPlaying = false;
            var iframe = $(elem).find('iframe');

            var player = new Vimeo.Player(iframe);
            player.on('ready', function() {
                player.setVolume(0);
            });

            player.on('play', function(){
                videoPlaying = true;
            })
            player.on('pause', function(){
                videoPlaying = false;
            })

            var scene = new ScrollMagic.Scene({
                triggerElement: elem,
                triggerHook: 0.9, // 0.9 - show, when scrolled 10% into view
                offset: 50, // move trigger to center of element
                reverse: true
            })
            .setClassToggle(elem, "playing") // add class toggle
            // .addIndicators() // add indicators (requires plugin)
            .on("start", function(e){
                elem.classList.add('run');
                appearedElements.push(elem);
                playVideo(player);
            })
            .on('enter', function(e){
                if(!videoPlaying) playVideo(player);
            })
            .on('leave', function(e){
                if(videoPlaying)  stopVideo(player);
            })
            .addTo(ScrollMagicController);
       }

       
       function playVideo(player){
            player.play().catch(function(error){
                console.log('play failed', error.name, error);
            });
       }
       function stopVideo(player){
        player.pause().catch(function(error){
            console.log('pause failed', error.name, error);
        });
       }
    }


    // doesnt work
    function homePageParalax(){
        // paralax01

        var elements = $('.paralax01');

        elements.each(function(index, elem){

            new ScrollMagic.Scene({
                triggerElement: elem,
                // triggerHook: 0.9, // 0.9 - show, when scrolled 10% into view
                offset: 50, // move trigger to center of element
                // reverse: false // only do once
                pushFollowers: false,
            })
            .setPin(elem)
            // .setClassToggle(elem, "run") // add class toggle
            // .addIndicators() // add indicators (requires plugin)
            .addTo(ScrollMagicController);

        });
    }

}
