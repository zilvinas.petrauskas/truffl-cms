function Flakes(container, config){
    
    this.config = config;
    this.container = container;

    this.inertia = 0.05; // amount of slow down per tick
    this.currentInertia = 1;

    this.ticking = false;
    this.busy = false;

    this.activeAnimations = [];

    this.mouse = {}
    
}

Flakes.prototype.init = function(){
    this.bindEvents();
}

Flakes.prototype.bindEvents = function(){

    // window.addEventListener('mousemove', this.mouseMove.bind(this));
    if( isMobile() || window.innerWidth < 800 || isTablet() ){
        this.mobileOnScroll();
    }else{
        window.addEventListener('scroll', this.onScroll.bind(this));
       
    }
   

}

Flakes.prototype.mouseMove = function(e){

    var rect = this.container.getBoundingClientRect();

    // if mouse is above the middle of hero container, move flakes up, otherwise move em down

    var middle = {
        y : (rect.top + rect.height ) / 2,
        x : (rect.left + rect.width ) / 2,
    }
    
    var outside = rect.top + rect.height;

    if( e.pageY > outside ){
        return;
    }

    var percentage = {
        y : parseFloat((e.pageY - middle.y) / rect.height).toFixed(2),
        x : parseFloat((e.pageX - middle.x) / rect.width).toFixed(2),
    }
    
    var ratio = {
        y : 0.1,
        x : -0.1,
    }; // ratio of truffl image height on diff axis

    var i,y, x;

    for(i=0;i<this.config.length;i++){
        y = this.config[i].element.clientHeight * percentage.y * ratio.y;
        x = this.config[i].element.clientWidth * percentage.x * ratio.x;
        this.config[i].img.style.transform = 'translateY('+y+'px) translateX('+x+'px)';
    }
}

Flakes.prototype.onScroll = function(e){

    if( this.containerVisible(this.container) ){
        this.animation();
    }    
}

Flakes.prototype.mobileOnScroll = function(){

    var lastScroll = 0;
    var scope = this;
    var debug = $('#debugScroll');
    this.inertia = 0; 

    setInterval(checkForScrollChanges, 1000/30);

    function checkForScrollChanges(){
        
        var currentY = parseInt(window.scrollY);
        if(currentY < 0) currentY = 0; // IOS awesomness can make it negative
        if(lastScroll == currentY) return;

        if( scope.containerVisible(scope.container) ){
            if(debug.length) debug.text(currentY);
            lastScroll = currentY;
            var anim = scope.animation.bind(scope);
            anim();
        }

    }

}

Flakes.prototype.animation = function(){

    if(this.busy)return;
    this.busy = true;

    this.ticking = false;
    var percentageHidden = this.calcHiddenPercentageOfElement(this.container);

    if(percentageHidden >= 1){
        this.busy = false;
        return;
    }

    var y2;

    for(let i=0;i< this.config.length;i++){
        y2 = this.calcTransformY(i, percentageHidden)
        this.config[i].element.style.transform = 'translateY('+y2+'px)';
    }

    if(this.inertia > 0){

        cancelAnimationFrame( this.activeAnimations.inertia );
        this.ticking = true;
        this.currentInertia = 1;
        
        this.animateWithInertion();
    }

    this.busy = false;
    return;
}

Flakes.prototype.calcHiddenPercentageOfElement = function(element){
    var rect = element.getBoundingClientRect();
    var percentageVisible =  parseFloat( (rect.height + rect.y) / rect.height ).toFixed(2);
    return 1 - percentageVisible;
}

Flakes.prototype.calcTransformY = function(i, percentageHidden){
    var slowDown = -5;
    var y2 = parseInt( this.config[i].element.clientHeight * this.config[i].ratio / slowDown * percentageHidden  );
    return y2;
}

Flakes.prototype.animateWithInertion = function(){

    this.currentInertia -= this.inertia;

    if( ! this.ticking || this.currentInertia <= 0 ) return;

    var percentageHidden = this.calcHiddenPercentageOfElement(this.container);
    var y2, inertia;

    for(var i=0;i< this.config.length;i++){

        y2 = this.calcTransformY(i, percentageHidden);
        inertia = y2 + Math.abs(y2 * this.currentInertia / 10);
       
        this.config[i].element.style.transform = 'translateY('+inertia+'px)';
    }
    
    if(this.ticking) this.activeAnimations.inertia = requestAnimationFrame(this.animateWithInertion.bind(this));

}

Flakes.prototype.getScrollPercent = function(){
    var h = document.documentElement, 
        b = document.body,
        st = 'scrollTop',
        sh = 'scrollHeight';
    return (h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight) * 100;
}

Flakes.prototype.containerVisible = function(element) {

    var rect = element.getBoundingClientRect();
    var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    var below = !(rect.bottom < 0);
    var above = !(rect.top - viewHeight >= 0);

    return  above && below;
}