
var HOME_DEBUG_INDICATORS = false;

document.addEventListener('DOMContentLoaded', function(){

    if( notHomePage() ) return;

    if( ! isLocalhost() ) HOME_DEBUG_INDICATORS = false;
    scrollMagicAnimations();
    animatedPartnersLogos({
        elements: document.querySelectorAll('.animated-partners-logos .logo-animated'),
    });
    flakesAnimation();
   
});

window.addEventListener('load', function () {

    if( notHomePage() ) return;
})

function notHomePage(){
    return $('.home').length === 0;
}



function scrollMagicAnimations(){

   
    var vh = window.innerHeight;
    var vw = window.innerWidth;

   
  
    // auto calling fn
    (function scrollMagichorizontalScroll(){

        var elm = document.querySelectorAll('#scrollHwhileScrollingV')[0];
        var scrollableAreaWidth = elm.scrollLeftMax || elm.scrollWidth - elm.clientWidth;

        var elmHeight = elm.clientHeight || elm.offsetHeight || elm.scrollHeight;
        var triggerDesktop = {
            duration: vh - elmHeight,
            offset: - vh * 0.15,
        }

        var triggerMobile = {
            duration: vh - elmHeight - ($('.header-container').height() * 1.5),
            offset: - vh * 0.33,
        }

        var trigger = window.innerWidth > 800 ? triggerDesktop : triggerMobile;
    
        var scene1 = new ScrollMagic.Scene({
            triggerElement: "#scrollHwhileScrollingV", 
            duration: trigger.duration,
            offset: trigger.offset,
        })
        .setTween(
            new TimelineMax ()
            .add([
                // TweenMax.to(".secret-ingredient .vertical-text-wrapper .vertical-text", 1, { y: vh * 0.2, ease: Linear.easeNone}),
                TweenMax.to("#scrollHwhileScrollingV", 1, { x: -scrollableAreaWidth, ease: Linear.easeNone}),
            ])
        )
        .addTo(ScrollMagicController);
        if(HOME_DEBUG_INDICATORS) scene1.addIndicators();
    
    })();

    (function mobileOnlyScrollRevealProjectsHover(){

        if(window.innerWidth > 800) return;

        var elements = document.querySelectorAll('.projects-1 .project-block');
        for(let i=0;i<elements.length;i++){
            triggerProjectHover(elements[i]);
        }

        function triggerProjectHover(element){

            var currentElement = '#' + element.getAttribute('id');

            var scene = new ScrollMagic.Scene({
                triggerElement: currentElement,
                offset: vw * 0.49,
            })
            .setClassToggle(currentElement, 'hover')
            .addTo(ScrollMagicController);
            if(HOME_DEBUG_INDICATORS) scene.addIndicators();
        }
    })();

    (function mobileOnlyTriggerHoverEffects(){

        if(window.innerWidth > 800) return;

        var elements = document.querySelectorAll('.projects .link-with-underline');
        for(let i=0;i<elements.length;i++){
            triggerLinkHover(elements[i]);
        }

        function triggerLinkHover(element){

            var scene = new ScrollMagic.Scene({
                triggerElement: element,
                offset: vw * 0.25,
            })
            .setClassToggle(element, 'hover')
            .addTo(ScrollMagicController);
            if(HOME_DEBUG_INDICATORS) scene.addIndicators();
        }

    })();
    
    (function animateBigLetters(){
        var scene = new ScrollMagic.Scene({
            triggerElement: "#weDigExplore",
            offset: -window.innerHeight / 5,
        })
        // trigger animation by adding a css class
        .setClassToggle(".letters-with-shadow", "progress")
        .addTo(ScrollMagicController);
    })();
}



function initHorScroll(){

    new horizontalScrolling([
        {
            element: document.querySelectorAll('.horizontally-scrollable')[0],
        },
    ]);
}

function flakesAnimation(){

    var flakesContainer = document.querySelectorAll('.hero')[0];

    var flakesConfig = [
        {
            element: document.querySelectorAll('.truffl-silver-1')[0],
            img: document.querySelectorAll('.truffl-silver-1 .truffl-silver-bg')[0],
            active: false,
            ratio: 10,
        },
        {
            element: document.querySelectorAll('.truffl-silver-2')[0],
            img: document.querySelectorAll('.truffl-silver-2 .truffl-silver-bg')[0],
            active: false,
            ratio: 3,
        },
        {
            element: document.querySelectorAll('.truffl-silver-3')[0],
            img: document.querySelectorAll('.truffl-silver-3 .truffl-silver-bg')[0],
            active: false,
            ratio: 8,
        }
    ];

    var homeFlakes = new Flakes(flakesContainer, flakesConfig);
    homeFlakes.init();
    

}

