// This configures the apostrophe-assets module to push a 'site.less'
// stylesheet by default, and to use jQuery 3.x

module.exports = {
  jQuery: 3,
  stylesheets: [
    { name: 'site' },
    { name: 'vendor/swiper' },
    // { name: 'vendor/select2' },
    { name: 'vendor/jquery.multiselect' },
  ],
  scripts: [
    { name: 'vendor/swiper' },
    { name: 'vendor/TweenMax.min' },
    { name: 'vendor/ScrollMagic.min' },
    { name: 'vendor/animation.gsap' },
    { name: 'vendor/debug.addIndicators' },
    { name: 'vendor/jquery.validate.min' },
    { name: 'vendor/jquery.fittext' },
    // { name: 'vendor/select2.min' },
    { name: 'vendor/jquery.multiselect' },
    { name: 'full-page-popup' },
    { name: 'dynamicPageLoad' },
    { name: 'isEventSupported' },
    { name: 'normalizedSpin' },
    { name: 'UserAgent_DEPRECATED' },
    { name: 'global' },
    { name: 'about' },
    { name: 'caselist' },
    { name: 'case' },
    { name: 'flakes' },
    { name: 'home' },
    { name: 'raph' },
  ]
};
