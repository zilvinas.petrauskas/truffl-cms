// This configures the apostrophe-pages module to add a "home" page type to the
// pages menu

module.exports = {
    types: [
      {
        name: 'home',
        label: 'Home Page',
      },
      {
        name: 'caselist',
        label: 'Our Work',
      },
      {
        name: 'case',
        label: 'Case Study Page'
      },
      {
        name: 'about',
        label: 'About page'
      },
      {
        name: 'raph',
        label: 'Meet Raph'
      },
      {
        name: 'contact',
        label: 'Contact'
      },
  
      // Add more page types here, but make sure you create a corresponding
      // template in lib/modules/apostrophe-pages/views/pages!
    ],
    
    filters:{
      ancestors: {
        children: {
          depth : 1,
          areas: ['defaultCasePageHeaderImage', 'image','images','thumbnail','apostrophe-images', 'drawer-case-header-image'],
        },
      },
      children: {
        areas: ['defaultCasePageHeaderImage', 'image','images','thumbnail','apostrophe-images', 'drawer-case-header-image'],
      }, // case page doesnt have any children pages
    },

    // filters: {
    //   ancestors: {
    //     children: {
    //       areas: [ 'image' ]
    //     }
    //   },
    //   children: {
    //     depth: 2,
    //     areas: [ 'image' ] // todo for optimization only load headerImage
    //   }
    // }
  };
  